package com.revature.testing;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.revature.dao.ReimbursementDaoImp;
import com.revature.model.Reimbursement;

public class DaoImplementationTest {
	@Test
	public void testSelectAllReimbursement() {
		List<Reimbursement> allList = new ArrayList<Reimbursement>(ReimbursementDaoImp.getInstance().selectAll());
		Assert.assertNotNull(allList);

	}

	@Test
	public void testSelectAllWaitingReimbursement() {
		List<Reimbursement> allWaitList = new ArrayList<Reimbursement>(
				ReimbursementDaoImp.getInstance().selectAllWaiting());
		Assert.assertNotNull(allWaitList);
	}

	@Test
	public void testSelectAllResolvedReimbursement() {
		List<Reimbursement> allResolvedList = new ArrayList<Reimbursement>(
				ReimbursementDaoImp.getInstance().selectAllResolved());
		Assert.assertNotNull(allResolvedList);
	}

	@Test
	public void testSelectSingleEmployeeAll() {
		Reimbursement tester = new Reimbursement();
		tester.setReimb_author(46);
		List<Reimbursement> allList = new ArrayList<Reimbursement>(
				ReimbursementDaoImp.getInstance().selectSingleEmployeeAll(tester));
		Assert.assertNotNull(allList);
	}
	
	@Test
	public void testSelectSingleEmployeeWaiting() {
		Reimbursement tester = new Reimbursement();
		tester.setReimb_author(46);
		List<Reimbursement> allWaitList = new ArrayList<Reimbursement>(
				ReimbursementDaoImp.getInstance().selectSingleEmployeeAllWaiting(tester));
		Assert.assertNotNull(allWaitList);
	}
	@Test
	public void testSelectSingleEmployeeResolved() {
		Reimbursement tester = new Reimbursement();
		tester.setReimb_author(46);
		List<Reimbursement> allResolvedList = new ArrayList<Reimbursement>(
				ReimbursementDaoImp.getInstance().selectSingleEmployeeAllResolved(tester));
		Assert.assertNotNull(allResolvedList);
	}
}
