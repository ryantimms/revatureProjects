package com.revature.testng;

import org.testng.annotations.Test;

import com.revature.factory.AssessmentsPage;
import com.revature.factory.HomePage;
import com.revature.factory.LoginPage;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class AssessmentsTest {
	WebDriver driver;
	Properties properties = new Properties();
	
	@Test(priority = 3)
	public void dropDownTest() {
		AssessmentsPage aPage= new AssessmentsPage(driver);
		aPage.displayDropDown.sendKeys(Keys.ARROW_DOWN);
		Assert.assertEquals(aPage.displayDropDown.getAttribute("value"), "200");
	}
	
	@Test(priority=1)
	public void basicSearchTest() {
		AssessmentsPage aPage= new AssessmentsPage(driver);
		String searchQuery= properties.getProperty("searchQuery");
		aPage.searchBar.sendKeys(searchQuery);
		searchQuery=searchQuery.toUpperCase();
		String linkText = aPage.assessmentNameLink.getText().toUpperCase();
		Assert.assertTrue(linkText.contains(searchQuery));
	}
	
	@Test (priority=2)
	public void characterLimitSearchTest() {
		AssessmentsPage aPage= new AssessmentsPage(driver);
		aPage.searchBar.clear();
		for(int x=0; x<500; x++) {
			aPage.searchBar.sendKeys("a");
		}
		Assert.assertEquals(aPage.searchBar.getAttribute("value").length(), 500);
	}
	
	@Test(priority=4)
	public void htmlInSearchBarTest() {
		AssessmentsPage aPage= new AssessmentsPage(driver);
		aPage.searchBar.clear();
		aPage.searchBar.sendKeys(properties.getProperty("htmlSearchQuery"));
		aPage.searchBar.click();
		System.out.println(driver.getTitle());
		Assert.assertEquals(driver.getTitle(), properties.getProperty("assessmentPageTitle"));
	}
	
	@Test(dataProvider = "dp")
	public void f(Integer n, String s) {
	}

	@BeforeMethod
	public void beforeMethod() {
	}

	@AfterMethod
	public void afterMethod() {
	}

	@DataProvider
	public Object[][] dp() {
		return new Object[][] { new Object[] { 1, "a" }, new Object[] { 2, "b" }, };
	}

	@BeforeClass
	public void beforeClass() throws IOException {
		File configFile = new File("src/main/resources/config.properties");
		FileInputStream fileInput = new FileInputStream(configFile);
		properties.load(fileInput);
		fileInput.close();

		File file = new File("C:\\Automation\\chromedriver_win32\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://preview.revature.com/core/");
		LoginPage loginPage = new LoginPage(driver);
		loginPage.username.clear();
		loginPage.password.clear();
		loginPage.username.sendKeys(properties.getProperty("username"));
		loginPage.password.sendKeys(properties.getProperty("password"));
		loginPage.submit.click();
		while (driver.getTitle().equals(properties.getProperty("loginPageTitle"))) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		HomePage hPage = new HomePage(driver);
		hPage.AssessmentsLink.click();
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

	@BeforeTest
	public void beforeTest() {
	}

	@AfterTest
	public void afterTest() {
	}

	@BeforeSuite
	public void beforeSuite() {
	}

	@AfterSuite
	public void afterSuite() {
	}

}
