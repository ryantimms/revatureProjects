#Author: Ryan Timms
#Keywords Summary : Test the display dropdown


@tag
Feature: Change the number of displayed assessments using a dropdown
	I want to use this template for my feature file

@tag2
Scenario Outline: Change the number of assessments displayed
Given I am logged in to an Admin account
And I am able to view the main assessment page
When I click a <number> from the display dropdown
Then The number of <rows> in the table is the same as the number chosen

Examples:
    | number | rows   |
    |  5     | 200    |
    |  2     | 100    |
		|  1		 | 0      |