package com.revature.factory;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AllQuestions {

	public AllQuestions(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[@id=\"qsnMgmtForm:tblselectmenu\"]")
	public WebElement DisplayDropdown;

	@FindBy(xpath = "//*[@id=\"qsnMgmtForm:qsnViewTblId:0:qsnViewBtnId\"]")
	public WebElement QuestionsSelection;

	@FindBy(xpath = "//*[@id=\"qsnMgmtForm:qsnListPanId\"]/div[1]/div/div[4]/a")
	public WebElement Reset;

	@FindBy(xpath = "//*[@id=\"qsnMgmtForm:qsnViewTblId_filter\"]/label/input")
	public WebElement Search;

	@FindBy(xpath = "//*[@id=\"qsnMgmtForm\"]/div/div[1]/div[1]/a")
	public WebElement CreateNew;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:qsnViewTblId\"]/tbody/tr[1]/td[3]/div")
	public WebElement Tags;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:qsnViewTblId\"]/tbody/tr")
	public List<WebElement> rows;
	
	@FindBy(xpath="//*[@id=\"treeCloseX\"]")
	public WebElement closeQuestionButton;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:qsnViewTblId:1:delQsnBtn\"]")
	public WebElement deleteQuestionButton;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:qsnViewTblId\"]/tbody/tr[1]/td[6]/div/a[1]")
	public WebElement editQuestionButton;
}
