<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="ersApp">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<link href="background.css" rel="stylesheet">
<script type="text/javascript" src="angular.min.js"></script>
<script type="text/javascript" src="controller.js"></script>

<title>Employees</title>
</head>
<body>
	<nav class="navbar">
	<div class="navbar navbar-inverse fixed-top">
		<div class="navbar-header">
			<a class="navbar-brand">Revature</a>
		</div>
		<ul class="nav navbar-nav">
			<li><a href="ManagerHome.html">Home</a></li>

			<li><a href="ManagerReimbHome.jsp">Reimbursements</a></li>

			<li><a href="viewManagerProfile.jsp">View Profile</a></li>
			
			<li><a href="#">View Employees</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="/EmployeeReimbursementSystem/LogOut.do">Log Out</a></li>
		</ul>
	</div>
	</nav>

	<div ng-controller="managerViewEmployeesController" class="container">
		<button ng-click="getDataFromServer()">View all Employees</button>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>User ID</th>
					<th>Username</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email Address</th>
			</thead>
			<tr ng-repeat="user in list">
				<td>{{user.user_id}}</td>
				<td>{{user.userName}}</td>
				<td>{{user.firstName}}</td>
				<td>{{user.lastName}}</td>
				<td>{{user.email}}</td>
		</table>
	</div>

</body>
</html>