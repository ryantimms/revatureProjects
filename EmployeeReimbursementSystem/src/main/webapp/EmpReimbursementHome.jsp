<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="ersApp">
<head>
<meta charset="ISO-8859-1">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<script type="text/javascript" src="angular.min.js"></script>
<script type="text/javascript" src="controller.js"></script>

<title>Employee Reimbursement Home Page</title>
</head>
<body>
	<nav class="navbar">
	<div class="navbar navbar-inverse fixed-top">
		<div class="navbar-header">
			<a class="navbar-brand">Revature</a>
		</div>
		<ul class="nav navbar-nav">
			<li><a href="home.html">Home</a></li>

			<li><a>Reimbursements</a></li>

			<li><a href="viewProfile.jsp">View Profile</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="/EmployeeReimbursementSystem/LogOut.do">Log Out</a></li>
		</ul>
	</div>

	</nav>

	<div class="jumbotron">
		<a href="newReimbursement.jsp">Click here to submit a new
			reimbursement ticket.</a><br>
	</div>
	<div ng-controller="empViewAllController" class="container">
		<button ng-click="getDataFromServer()">Click here to view
			full reimbursement history.</button>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Amount</th>
					<th>Description</th>
					<th>Author ID</th>
					<th>Resolver ID(43 if pending)</th>
					<th>Type(1 for Travel, 2 for Certification, 3 for Food)</th>
					<th>Status(1 for Approved, 2 for Denied, 3 for Pending)</th>
			</thead>
			<tr ng-repeat="r in list">
				<td>{{r.reimb_amount | currency}}</td>
				<td>{{r.reimb_description}}</td>
				<td>{{r.reimb_author}}</td>
				<td>{{r.reimb_resolver}}</td>
				<td>{{r.reimb_type_id}}</td>
				<td>{{r.reimb_status_id}}</td>
			</tr>
		</table>
	</div>
	
<div ng-controller="empViewPendingController" class="container">
		<button ng-click="getDataFromServer()">Click here to view
			pending reimbursements.</button>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Amount</th>
					<th>Description</th>
					<th>Author ID</th>
					<th>Resolver ID(43 if pending)</th>
					<th>Type(1 for Travel, 2 for Certification, 3 for Food)</th>
					<th>Status(1 for Approved, 2 for Denied, 3 for Pending)</th>
			</thead>
			<tr ng-repeat="r in list">
				<td>{{r.reimb_amount | currency}}</td>
				<td>{{r.reimb_description}}</td>
				<td>{{r.reimb_author}}</td>
				<td>{{r.reimb_resolver}}</td>
				<td>{{r.reimb_type_id}}</td>
				<td>{{r.reimb_status_id}}</td>
			</tr>
		</table>
	</div>
</body>
</html>