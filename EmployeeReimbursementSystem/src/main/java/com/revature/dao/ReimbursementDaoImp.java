package com.revature.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.revature.model.Reimbursement;
import com.revature.model.User;
import com.revature.util.ConnectionUtil;
import com.revature.util.LogUtil;

public class ReimbursementDaoImp implements ReimbursementDao {

	// private constructor for singleton implementation
	private ReimbursementDaoImp() {
	}

	private static ReimbursementDaoImp reimbursementDaoImp;

	public static ReimbursementDaoImp getInstance() {
		if (reimbursementDaoImp == null) {
			reimbursementDaoImp = new ReimbursementDaoImp();
		}
		return reimbursementDaoImp;
	}

	@Override
	public List<Reimbursement> selectAll() {
		try (Connection conn = ConnectionUtil.getConnection()) {

			String command = "SELECT * FROM REIMBURSEMENT";
			PreparedStatement statement = conn.prepareStatement(command);
			ResultSet result = statement.executeQuery();
			List<Reimbursement> allTickets = new ArrayList<Reimbursement>();
			while (result.next()) {
				allTickets.add(new Reimbursement(result.getInt("REIMB_ID"), result.getDouble("REIMB_AMOUNT"),
						result.getInt("REIMB_AUTHOR"), result.getInt("REIMB_RESOLVER"),
						result.getInt("REIMB_STATUS_ID"), result.getInt("REIMB_TYPE_ID")));
			}
			return allTickets;
		} catch (SQLException e) {
			// LogUtil.logger.warn("Exception selecting a user.", e);
		}
		return null;
	}

	@Override
	public List<Reimbursement> selectAllWaiting() {
		try (Connection conn = ConnectionUtil.getConnection()) {

			String command = "SELECT * FROM REIMBURSEMENT WHERE REIMB_STATUS_ID=3";
			PreparedStatement statement = conn.prepareStatement(command);
			ResultSet result = statement.executeQuery();
			List<Reimbursement> allTickets = new ArrayList<Reimbursement>();
			while (result.next()) {
				allTickets.add(new Reimbursement(result.getInt("REIMB_ID"), result.getDouble("REIMB_AMOUNT"),
						result.getInt("REIMB_AUTHOR"), result.getInt("REIMB_RESOLVER"),
						result.getInt("REIMB_STATUS_ID"), result.getInt("REIMB_TYPE_ID"),
						result.getString("REIMB_DESCRIPTION")

				));
			}
			return allTickets;
		} catch (SQLException e) {
			// LogUtil.logger.warn("Exception selecting a user.", e);
		}
		return null;
	}

	@Override
	public List<Reimbursement> selectAllResolved() {
		try (Connection conn = ConnectionUtil.getConnection()) {

			String command = "SELECT * FROM REIMBURSEMENT WHERE REIMB_STATUS_ID<3";
			PreparedStatement statement = conn.prepareStatement(command);
			ResultSet result = statement.executeQuery();
			List<Reimbursement> allTickets = new ArrayList<Reimbursement>();
			while (result.next()) {
				allTickets.add(new Reimbursement(result.getInt("REIMB_ID"), result.getDouble("REIMB_AMOUNT"),
						result.getInt("REIMB_AUTHOR"), result.getInt("REIMB_RESOLVER"),
						result.getInt("REIMB_STATUS_ID"), result.getInt("REIMB_TYPE_ID"),
						result.getString("REIMB_DESCRIPTION")));
			}
			return allTickets;
		} catch (SQLException e) {
			// LogUtil.logger.warn("Exception selecting a user.", e);
		}
		return null;
	}

	@Override
	public List<Reimbursement> selectSingleEmployeeAll(Reimbursement reimbursement) {
		try (Connection conn = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			String command = "SELECT * FROM REIMBURSEMENT WHERE REIMB_AUTHOR=?";
			PreparedStatement statement = conn.prepareStatement(command);
			statement.setInt(++statementIndex, reimbursement.getReimb_author());
			ResultSet result = statement.executeQuery();
			List<Reimbursement> allTickets = new ArrayList<Reimbursement>();
			while (result.next()) {
				allTickets.add(new Reimbursement(result.getInt("REIMB_ID"), result.getDouble("REIMB_AMOUNT"),
						result.getInt("REIMB_AUTHOR"), result.getInt("REIMB_RESOLVER"),
						result.getInt("REIMB_STATUS_ID"), result.getInt("REIMB_TYPE_ID"),
						result.getString("REIMB_DESCRIPTION")));
			}
			return allTickets;
		} catch (SQLException e) {
			// LogUtil.logger.warn("Exception selecting a user.", e);
		}
		return null;
	}
	public List<Reimbursement> selectSingleEmployeeAll(User user){
		try (Connection conn = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			String command = "SELECT * FROM REIMBURSEMENT WHERE REIMB_AUTHOR=?";
			PreparedStatement statement = conn.prepareStatement(command);
			statement.setInt(++statementIndex, user.getUser_id());
			ResultSet result = statement.executeQuery();
			List<Reimbursement> allTickets = new ArrayList<Reimbursement>();
			while (result.next()) {
				allTickets.add(new Reimbursement(result.getInt("REIMB_ID"), result.getDouble("REIMB_AMOUNT"),
						result.getInt("REIMB_AUTHOR"), result.getInt("REIMB_RESOLVER"),
						result.getInt("REIMB_STATUS_ID"), result.getInt("REIMB_TYPE_ID"),
						result.getString("REIMB_DESCRIPTION")));
			}
			System.out.println("fromDaoImp"+ allTickets);
			return allTickets;
		} catch (SQLException e) {
			// LogUtil.logger.warn("Exception selecting a user.", e);
		}
		return null;
	}

	@Override
	public List<Reimbursement> selectSingleEmployeeAllWaiting(Reimbursement reimbursement) {
		try (Connection conn = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			String command = "SELECT * FROM REIMBURSEMENT WHERE REIMB_AUTHOR=? AND REIMB_STATUS_ID=3";
			PreparedStatement statement = conn.prepareStatement(command);
			statement.setInt(++statementIndex, reimbursement.getReimb_author());
			ResultSet result = statement.executeQuery();
			List<Reimbursement> allTickets = new ArrayList<Reimbursement>();
			while (result.next()) {
				allTickets.add(new Reimbursement(result.getInt("REIMB_ID"), result.getDouble("REIMB_AMOUNT"),
						result.getInt("REIMB_AUTHOR"), result.getInt("REIMB_RESOLVER"),
						result.getInt("REIMB_STATUS_ID"), result.getInt("REIMB_TYPE_ID"),
						result.getString("REIMB_DESCRIPTION")));
			}
		} catch (SQLException e) {
			 LogUtil.logger.warn("Exception selecting a user.", e);
		}
		return null;
	}
	public List<Reimbursement> selectSingleEmployeeAllWaiting(User user) {
		try (Connection conn = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			String command = "SELECT * FROM REIMBURSEMENT WHERE REIMB_AUTHOR=? AND REIMB_STATUS_ID=3";
			PreparedStatement statement = conn.prepareStatement(command);
			statement.setInt(++statementIndex, user.getUser_id());
			ResultSet result = statement.executeQuery();
			List<Reimbursement> allTickets = new ArrayList<Reimbursement>();
			while (result.next()) {
				allTickets.add(new Reimbursement(result.getInt("REIMB_ID"), result.getDouble("REIMB_AMOUNT"),
						result.getInt("REIMB_AUTHOR"), result.getInt("REIMB_RESOLVER"),
						result.getInt("REIMB_STATUS_ID"), result.getInt("REIMB_TYPE_ID"),
						result.getString("REIMB_DESCRIPTION")));
			}
		} catch (SQLException e) {
			 LogUtil.logger.warn("Exception selecting a user.", e);
		}
		return null;
	}

	@Override
	public List<Reimbursement> selectSingleEmployeeAllResolved(Reimbursement reimbursement) {
		try (Connection conn = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			String command = "SELECT * FROM REIMBURSEMENT WHERE REIMB_AUTHOR_ID=? AND REIMB_STATUS_ID<3";
			PreparedStatement statement = conn.prepareStatement(command);
			statement.setInt(++statementIndex, reimbursement.getReimb_author());
			ResultSet result = statement.executeQuery();
			List<Reimbursement> allTickets = new ArrayList<Reimbursement>();
			while (result.next()) {
				allTickets.add(new Reimbursement(result.getInt("REIMB_ID"), result.getDouble("REIMB_AMOUNT"),
						result.getInt("REIMB_AUTHOR"), result.getInt("REIMB_RESOLVER"),
						result.getInt("REIMB_STATUS_ID"), result.getInt("REIMB_TYPE_ID"),
						result.getString("REIMB_DESCRIPTION")));
			}
		} catch (SQLException e) {
			 LogUtil.logger.warn("Exception selecting a user.", e);
		}
		return null;
	}

	@Override
	public boolean resolveReimbursement(Reimbursement reimbursement) {
		try (Connection conn = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			String command = "";
			System.out.println("from resolve reimbursement inside of daoimp"+reimbursement);
				command = "UPDATE REIMBURSEMENT SET REIMB_STATUS_ID=?, REIMB_RESOLVER=? WHERE REIMB_ID=?";
				PreparedStatement statement = conn.prepareStatement(command);
				statement.setInt(++statementIndex, reimbursement.getReimb_status_id());
				statement.setInt(++statementIndex, reimbursement.getReimb_resolver());
				statement.setInt(++statementIndex, reimbursement.getReimb_id());
				if (statement.executeUpdate() > 0) {
					return true;
				}
		} catch (SQLException e) {
			e.getStackTrace();
		}
		return false;
	}


	
	
	@Override
	public boolean insertReimbursementProc(Reimbursement reimbursement) {
		try (Connection conn = ConnectionUtil.getConnection()) {

			int statementIndex = 0;
			String command = "{CALL ADDREIMB(?,?,?,?,?)}";
			CallableStatement statement = conn.prepareCall(command);
			// Set attributes to be inserted
			statement.setDouble(++statementIndex, reimbursement.getReimb_amount());
			statement.setString(++statementIndex, reimbursement.getReimb_description());
			statement.setInt(++statementIndex, reimbursement.getReimb_author());
			statement.setInt(++statementIndex, reimbursement.getReimb_resolver());
			statement.setInt(++statementIndex, reimbursement.getReimb_type_id());
			
			
			//Executes the SQL command and returns the row count if successful, 0 if not
			if (statement.executeUpdate() >0) {
				return true;
			}
		} catch (SQLException e) {
//			LogUtil.logger.warn("Exception inserting new user.", e);
		}
		return false;	}

}
