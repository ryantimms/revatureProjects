#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios 
#<> (placeholder)
#""
## (Comments)

#Sample Feature Definition Template
@tag
Feature: Import Aiken Question
	As a trainer
	I want to import custom questions
	In order to challenge my trainees

@tag1
Scenario: Importing Aiken Format Questions
Given I am logged in as a trainer
	And am on the import questions page
When I correctly complete the aiken format import form
	And submit the form
Then a new question should be created 

@tag2
Scenario Outline: Title of your scenario outline
Given I want to write a step with <input>
When I check for the <value> in step
Then I verify the <status> in step

Examples:
    | input  |value | status |
    | validInput |  aiken.txt   | success|
    | invalidInput |  xmlFile.xml   | Fail   |
