package com.revature.controller;

import javax.servlet.http.HttpServletRequest;
import com.revature.model.User;
import com.revature.services.UserService;
import com.revature.util.FinalUtil;

public class RegisterController {
	public static String registerNewUser(HttpServletRequest request) {

		// Return to registration view for unsecured get method
		if (request.getMethod().equals(FinalUtil.HTTP_GET)) {
			return "newUser.jsp";
		}

		// POST method is secured
		// Check if username already exists
		User newUser = new User();
		newUser.setFirstName(request.getParameter("firstName"));
		newUser.setEmail(request.getParameter("email"));
		newUser.setLastName(request.getParameter("lastName"));
		newUser.setPassword(request.getParameter("inputPassword"));
		newUser.setUserName(request.getParameter("inputUsername"));
		newUser.setRole_id(request.getParameter("role"));
		if (UserService.getInstance().isUserNameTaken(newUser)) {
			return "newUser.jsp";
		}
		if (UserService.getInstance().registerUserProc(newUser)) {
			return "Login.jsp";
		} else {
			return "index.html";
		}
	}
}
