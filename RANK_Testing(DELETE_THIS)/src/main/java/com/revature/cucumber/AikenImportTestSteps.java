
package com.revature.cucumber;

import java.io.File;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.revature.factory.HomePage;
import com.revature.factory.ImportQuestionsPage;
import com.revature.factory.LoginPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AikenImportTestSteps {

	public static WebDriver driver;
	Properties properties = new Properties();

	SoftAssert softAssert = new SoftAssert();
	ImportQuestionsPage iq = new ImportQuestionsPage(driver);

	@Given("^I am logged in as a trainer$")
	public void i_am_logged_in_as_a_trainer() throws IOException {
		
		File configFile = new File("src/main/resources/config.properties");
		FileInputStream fileInput = new FileInputStream(configFile);
		properties.load(fileInput);
		fileInput.close();

		File file = new File("C:\\automation\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://preview.revature.com/core/");
		// Write code here that turns the phrase above into concrete actions
		LoginPage loginPage = new LoginPage(driver);
		loginPage.username.sendKeys(properties.getProperty("username"));
		loginPage.password.sendKeys(properties.getProperty("password"));
		loginPage.submit.click();
		while (driver.getTitle().equals("Revature | Admin")) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		System.out.println(driver.getTitle());
		Assert.assertNotEquals(driver.getTitle(), "Revature | Admin");

	}

	@Given("^am on the import questions page$")
	public void am_on_the_import_questions_page() {
		// Write code here that turns the phrase above into concrete actions
		HomePage hpage = new HomePage(driver);

		while (!hpage.QuestionsDropdown.isDisplayed()) {
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			if (hpage.QuestionsDropdown.isDisplayed()) {
				hpage.QuestionsDropdown.click();
				break;
			}
		}

		while (!hpage.ImportQuestionLink.isDisplayed()) {
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			if (hpage.ImportQuestionLink.isDisplayed()) {
				hpage.ImportQuestionLink.click();
				break;
			}
		}

	}

	@When("^I correctly complete the aiken format import form$")
	public void i_correctly_complete_the_aiken_format_import_form() {
		// Write code here that turns the phrase above into concrete actions

		SoftAssert softAssert = new SoftAssert();
		ImportQuestionsPage iq = new ImportQuestionsPage(driver);
		
		
		WebDriverWait wait = new WebDriverWait(driver, 8);
   	    wait.until(ExpectedConditions.visibilityOf(iq.AikenFormatRadio));
		iq.AikenFormatRadio.click();
        softAssert.assertEquals(iq.AikenFormatRadio.isSelected(), true);
        
        while(!iq.categoryDropdown.isEnabled()) {
 	    	 driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
 	    	 if(iq.categoryDropdown.isEnabled()) {
 	  	    	 System.out.println("categorydropdown is displayed.");
 	  	    	Select category = new Select(iq.categoryDropdown);
 	  			System.out.println("HELLLLLLLLLooooooooooooooooo"+iq.categoryDropdown.toString());
 	  			category.selectByIndex(5);
 	  			
 	  			WebElement selectedElement = category.getFirstSelectedOption();
 	  			String selectedOption = selectedElement.getText();
 	  			
 	  			softAssert.assertEquals(selectedOption.contains(properties.getProperty("categorySelect")), true);	
 	  			break;
 	  	     }
 	     }
        
        
        while(!iq.levelDropdown.isEnabled()) {
 	    	 driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
 	    	 if(iq.levelDropdown.isEnabled()) {
 	  	    	 System.out.println("categorydropdown is displayed.");
 	  	    	Select category = new Select(iq.levelDropdown);
 	  			System.out.println("HELLLLLLLLLooooooooooooooooo"+iq.levelDropdown.toString());
 	  			category.selectByIndex(2);
 	  			
 	  			WebElement selectedElement = category.getFirstSelectedOption();
 	  			String selectedOption = selectedElement.getText();
 	  			
 	  			softAssert.assertEquals(selectedOption.contains(properties.getProperty("categorySelect")), true);	
 	  			break;
 	  	     }
 	     }
        
        iq.skillPointsInput.sendKeys("20");
		softAssert.assertEquals(iq.skillPointsInput.getAttribute("value"), "20");
		
		iq.scoreInput.sendKeys("20");
		softAssert.assertEquals(iq.scoreInput.getAttribute("value"), "20");
		
		iq.chooseFileButtonAiken.sendKeys(properties.getProperty("validAiken"));

	}

	@When("^submit the form$")
	public void submit_the_form() {
		// Write code here that turns the phrase above into concrete actions
		iq.importButtonAiken.click();

	}

	@Then("^a new question should be created$")
	public void a_new_question_should_be_created() {
		// Write code here that turns the phrase above into concrete actions
		i_verify_the_success_in_step();
		iq.saveNewQs.click();

	}

	@Given("^I want to write a step with validInput$")
	public void i_want_to_write_a_step_with_validInput() {
		// Write code here that turns the phrase above into concrete actions
		iq.chooseFileButtonAiken.sendKeys(properties.getProperty("validAiken"));

	}

	@When("^I check for the aiken\\.txt in step$")
	public void i_check_for_the_aiken_txt_in_step() {
		// Write code here that turns the phrase above into concrete actions

		iq.hiddenInputOnFileChange.clear();
		String oldValue = iq.hiddenInputOnFileChange.getAttribute("value");

		iq.chooseFileButtonAiken.sendKeys(properties.getProperty("validAiken"));
		softAssert.assertNotEquals(iq.hiddenInputOnFileChange.getAttribute("value"), oldValue);

	}

	@Then("^I verify the success in step$")
	public void i_verify_the_success_in_step() {
		// Write code here that turns the phrase above into concrete actions
		i_correctly_complete_the_aiken_format_import_form();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		softAssert.assertEquals(iq.aikenErrorMsg.getText().contains(properties.getProperty("successMsg")), true);

	}

	@Given("^I want to write a step with invalidInput$")
	public void i_want_to_write_a_step_with_invalidInput() {
		// Write code here that turns the phrase above into concrete actions
		iq.chooseFileButtonAiken.sendKeys(properties.getProperty("sampleXML"));

	}

	@When("^I check for the xmlFile\\.xml in step$")
	public void i_check_for_the_xmlFile_xml_in_step() {
		// Write code here that turns the phrase above into concrete actions
		// hidden input will appear after keys are sent to file input

		iq.hiddenInputOnFileChange.clear();
		String oldValue = iq.hiddenInputOnFileChange.getAttribute("value");

		iq.chooseFileButtonAiken.sendKeys(properties.getProperty("sampleXML"));
		softAssert.assertNotEquals(iq.hiddenInputOnFileChange.getAttribute("value"), oldValue);

	}

	@Then("^I verify the Fail in step$")
	public void i_verify_the_Fail_in_step() {
		// Write code here that turns the phrase above into concrete actions
		softAssert.assertEquals(iq.aikenErrorMsg.getText().contains(properties.getProperty("successMsg")), false);

	}

}
