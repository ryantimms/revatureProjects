package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

import com.revature.model.User;
import com.revature.services.UserService;
import com.revature.util.FinalUtil;

public class UpdateController {
	public static String update(HttpServletRequest request) {
		
		//check for get method
		if (request.getMethod().equals(FinalUtil.HTTP_GET)) {
			return "viewProfile.jsp";
		}
		
		
		User loggedUser = (User) request.getSession().getAttribute("loggedUser");

		if (request.getParameter("firstName") != null) {
			loggedUser.setFirstName(request.getParameter("firstName"));
		}
		if (request.getParameter("lastName") != null) {
			loggedUser.setLastName(request.getParameter("lastName"));
		}
		if (request.getParameter("email") != null) {
			loggedUser.setEmail(request.getParameter("email"));
		}
		UserService.getInstance().updateUser(loggedUser);
		return "viewProfile.jsp";
	}
}
