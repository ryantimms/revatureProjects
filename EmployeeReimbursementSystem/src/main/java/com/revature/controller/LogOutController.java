package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

public class LogOutController {
public static String logout(HttpServletRequest request) {
	request.getSession().invalidate();
	return "Login.jsp";
}
}
