package com.revature.controller;

import java.util.List;

import com.revature.dao.ReimbursementDaoImp;
import com.revature.model.Reimbursement;
import com.revature.model.User;

public class ViewEmpReimbursementsController {
public static List<Reimbursement> showAll(User user){
	return ReimbursementDaoImp.getInstance().selectSingleEmployeeAll(user);
}
}
