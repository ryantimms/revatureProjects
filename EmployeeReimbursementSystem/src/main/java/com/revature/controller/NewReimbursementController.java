package com.revature.controller;


import javax.servlet.http.HttpServletRequest;

import com.revature.model.Reimbursement;
import com.revature.model.User;
import com.revature.services.ReimbursementService;
import com.revature.util.FinalUtil;

public class NewReimbursementController {
public static String submitNewTicket(HttpServletRequest request) {
	if (request.getMethod().equals(FinalUtil.HTTP_GET)) {
		return "newReimbursement.jsp";
	}
	User author = new User();
	author=(User) request.getSession().getAttribute("loggedUser");
	Reimbursement newReimb = new Reimbursement();
	newReimb.setReimb_amount(request.getParameter("amount"));
	newReimb.setReimb_description(request.getParameter("description"));
	newReimb.setReimb_author(author.getUser_id());
	newReimb.setReimb_resolver(43);
	newReimb.setReimb_type_id(request.getParameter("type"));
	newReimb.setReimb_status_id(3);
	if(ReimbursementService.getInstance().insertReimbProc(newReimb)) {
		return "EmpReimbursementHome.jsp";
	}
	else {
		return "newReimbursement.jsp";
	}
}
}
