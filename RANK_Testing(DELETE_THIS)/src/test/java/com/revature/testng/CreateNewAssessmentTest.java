package com.revature.testng;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.revature.factory.AssessmentsCreateNew;
import com.revature.factory.AssessmentsPage;
import com.revature.factory.HomePage;
import com.revature.factory.LoginPage;


public class CreateNewAssessmentTest {
	Properties properties = new Properties();
	WebDriver driver;
	AssessmentsCreateNew aCreateNew;

	
	//TODO: S&C
	@Test (enabled = true)
	public void testAssessmentName() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.assessmentNameInput.clear();
		aCreateNew.assessmentNameInput.sendKeys(properties.getProperty("standardName"));
		aCreateNew.saveContinueButton.click();
		softAssert.assertEquals(aCreateNew.assessmentNameInput.getText(), properties.getProperty("standardName"));
	}
	//TODO: S&C - needs all required fields for test
	@Test (enabled = false)
	public void testAssessmentNameEmpty() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.assessmentNameInput.clear();
		aCreateNew.assessmentNameInput.sendKeys(Keys.RETURN);
		aCreateNew.saveContinueButton.click();
		//should stay on same page & not let you save
		softAssert.assertEquals(driver.getCurrentUrl(), "https://preview.revature.com/core/admin/pages/assessments?add");
	}
	
	@Test (enabled = true)
	public void testNegativeActivityPoints() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.activityPointsInput.clear();
		aCreateNew.activityPointsInput.sendKeys(properties.getProperty("negativeNum"));
		softAssert.assertNotEquals(aCreateNew.activityPointsInput.getText(), properties.getProperty("negativeNum"));
	}
	@Test (enabled = true)
	public void testDecimalActivityPoints() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.activityPointsInput.clear();
		aCreateNew.activityPointsInput.sendKeys(properties.getProperty("decimalNum"));
		softAssert.assertNotEquals(aCreateNew.activityPointsInput.getText(), properties.getProperty("decimalNum"));
	}
	
	@Test (enabled = true)
	public void testLettersInActivityPoints() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.activityPointsInput.clear();
		aCreateNew.activityPointsInput.sendKeys(properties.getProperty("lettersTest"));
		softAssert.assertNotEquals(aCreateNew.activityPointsInput.getText(), properties.getProperty("lettersTest"));
	}
	
	//TODO: S&C
	@Test (enabled = true)
	public void testOctalActivityPoints() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.activityPointsInput.clear();
		aCreateNew.activityPointsInput.sendKeys(properties.getProperty("octalNum"));
		aCreateNew.saveContinueButton.click();
		softAssert.assertEquals(aCreateNew.activityPointsInput.getText(), properties.getProperty("octalNumWanted"));
	}
	
	@Test (enabled = true)
	public void testDownButtonToZeroMaxAttempts() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.maxNoOfAttemptsInput.clear();
		aCreateNew.maxNoOfAttemptsInput.sendKeys(properties.getProperty("one"));
		aCreateNew.maxNoAttemptsDownButton.click();
		softAssert.assertNotEquals(aCreateNew.maxNoOfAttemptsInput.getText(), properties.getProperty("zero"));
	}
	
	@Test (enabled = true)
	public void testUpButtonMaxAttempts() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.maxNoOfAttemptsInput.clear();
		aCreateNew.maxNoOfAttemptsInput.sendKeys(properties.getProperty("one"));
		aCreateNew.maxNoAttemptsUpButton.click();
		softAssert.assertEquals(aCreateNew.maxNoOfAttemptsInput.getText(), properties.getProperty("two"));
	}
	
	@Test (enabled = true)
	public void testDownButtonMaxAttempts() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.maxNoOfAttemptsInput.clear();
		aCreateNew.maxNoOfAttemptsInput.sendKeys(properties.getProperty("two"));
		aCreateNew.maxNoAttemptsUpButton.click();
		softAssert.assertEquals(aCreateNew.maxNoOfAttemptsInput.getText(), properties.getProperty("one"));
	}
	
	@Test (enabled = true)
	public void testDescriptionLimit() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.descriptionTextArea.clear();
		for (int i = 0; i < 101; i++) {
			aCreateNew.descriptionTextArea.sendKeys("aAaAaAaAaAaAaAaAaAaAa");
		}
		softAssert.assertNotEquals(aCreateNew.descriptionTextArea.getAttribute("value").length(), "overMaxTextSize");
		softAssert.assertEquals(aCreateNew.descriptionTextArea.getAttribute("value").length(), "maxTextSize");
		aCreateNew.descriptionTextArea.clear();
	}
	
	@Test (enabled = true)
	public void testDownButtonPassPercent() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.passPercentInput.clear();
		aCreateNew.passPercentInput.sendKeys(properties.getProperty("two"));
		aCreateNew.passPercentDownButton.click();
		softAssert.assertNotEquals(aCreateNew.passPercentInput.getText(), properties.getProperty("two"));
		softAssert.assertEquals(aCreateNew.passPercentInput.getText(), properties.getProperty("one"));
	}
	
	@Test (enabled = true)
	public void testUpButtonPassPercent() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.passPercentInput.clear();
		aCreateNew.passPercentInput.sendKeys(properties.getProperty("one"));
		aCreateNew.passPercentUpButton.click();
		softAssert.assertNotEquals(aCreateNew.passPercentInput.getText(), properties.getProperty("one"));
		softAssert.assertEquals(aCreateNew.passPercentInput.getText(), properties.getProperty("two"));
	}
	
	@Test (enabled = true)
	public void testZeroPassPercent() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.passPercentInput.clear();
		aCreateNew.passPercentInput.sendKeys(properties.getProperty("one"));
		aCreateNew.passPercentDownButton.click();
		softAssert.assertEquals(aCreateNew.passPercentInput.getText(), properties.getProperty("one"));
		softAssert.assertNotEquals(aCreateNew.passPercentInput.getText(), properties.getProperty("zero"));
	}
	
	@Test (enabled = true)
	public void testOverHundredPassPercent() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.passPercentInput.clear();
		aCreateNew.passPercentInput.sendKeys(properties.getProperty("oneHundred"));
		aCreateNew.passPercentUpButton.click();
		softAssert.assertEquals(aCreateNew.passPercentInput.getText(), properties.getProperty("oneHundred"));
		softAssert.assertNotEquals(aCreateNew.passPercentInput.getText(), properties.getProperty("oneHundredAndOne"));
	}
	
	@Test (enabled = true)
	public void testNotSelectingLevel() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		Select levelDropdown = new Select(aCreateNew.levelDropdown);
		levelDropdown.selectByVisibleText(properties.getProperty("selectOption"));
		aCreateNew.saveContinueButton.click();
		softAssert.assertEquals(driver.getCurrentUrl(), "https://preview.revature.com/core/admin/pages/assessments?add");
		softAssert.assertNotEquals(driver.getCurrentUrl(), "https://preview.revature.com/core/admin/pages/assessments");
	}
	
	@Test (enabled = true)
	public void testOverrideLevelOff() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.levelOverrideOffRadio.click();
		softAssert.assertTrue(aCreateNew.levelOverrideOffRadio.isSelected());
		softAssert.assertFalse(aCreateNew.addQuestionsButton.isEnabled());
	}
	
	@Test (enabled = true)
	public void testOverrideLevelOn() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.levelOverrideOnRadio.click();
		softAssert.assertTrue(aCreateNew.levelOverrideOnRadio.isSelected());
		softAssert.assertTrue(aCreateNew.addQuestionsButton.isEnabled());
	}
	
	@Test (enabled = false)
	public void testMetaDescriptionLimit() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.metaDescriptionTextArea.clear();
		for (int i = 0; i < 101; i++) {
			aCreateNew.descriptionTextArea.sendKeys("aAaAaAaAaAaAaAaAaAaAa");
		}
		softAssert.assertNotEquals(aCreateNew.descriptionTextArea.getAttribute("value").length(), properties.getProperty("overMaxTextSize"));
		softAssert.assertEquals(aCreateNew.descriptionTextArea.getAttribute("value").length(), properties.getProperty("maxTextSize"));
		aCreateNew.metaDescriptionTextArea.clear();
	}
	
	@Test (enabled = true)
	public void testDescriptionMetaDescriptionConnection() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.descriptionTextArea.clear();
		aCreateNew.metaDescriptionTextArea.clear();
		aCreateNew.descriptionTextArea.sendKeys(properties.getProperty("RANKtag"));
		aCreateNew.descriptionTextArea.sendKeys(Keys.SPACE);
		aCreateNew.descriptionTextArea.sendKeys(properties.getProperty("selectOption"));
		aCreateNew.metaDescriptionTextArea.click();
		softAssert.assertEquals(aCreateNew.descriptionTextArea.getText(), properties.getProperty("descriptionOrdered"));
		softAssert.assertEquals(aCreateNew.metaDescriptionTextArea.getText(), properties.getProperty("descriptionOrdered"));
		aCreateNew.descriptionTextArea.clear();
		aCreateNew.metaDescriptionTextArea.clear();
	}
	
	
	
	@Test (enabled = true)
	public void testDescriptionMetaDescriptionReverseConnection() {
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.descriptionTextArea.clear();
		aCreateNew.metaDescriptionTextArea.clear();
		aCreateNew.descriptionTextArea.sendKeys(properties.getProperty("RANKtag"));
		aCreateNew.descriptionTextArea.sendKeys(Keys.SPACE);
		aCreateNew.descriptionTextArea.sendKeys(properties.getProperty("selectOption"));
		aCreateNew.metaDescriptionTextArea.click();
		aCreateNew.metaDescriptionTextArea.sendKeys(properties.getProperty("lettersTest"));
		aCreateNew.descriptionTextArea.sendKeys(Keys.SPACE);
		aCreateNew.descriptionTextArea.sendKeys(properties.getProperty("ACNtag"));
		aCreateNew.metaDescriptionTextArea.click();
		Assert.assertEquals(aCreateNew.descriptionTextArea.getText(), properties.getProperty("descriptionUnordered"));
		Assert.assertNotEquals(aCreateNew.metaDescriptionTextArea.getText(), properties.getProperty("descriptionUnordered"));
		aCreateNew.descriptionTextArea.clear();
		aCreateNew.metaDescriptionTextArea.clear();
	}
	

	@Test (enabled = true)
	public void testSlugWithPoundSign() {
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.slugInput.clear();
		aCreateNew.slugInput.sendKeys(properties.getProperty("poundSignHello"));
		aCreateNew.saveContinueButton.click();
		//shouldn't allow a #
		Assert.assertNotEquals(aCreateNew.slugInput.getAttribute("value"), properties.getProperty("poundSignHello"));
	}
	
	@Test (enabled = true)
	public void testSlugLimit() {
		SoftAssert softAssert = new SoftAssert();
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.slugInput.clear();
		for (int i = 0; i < 6; i++) {
			aCreateNew.slugInput.sendKeys("aAaAaAaAaAaAaAaAaAaAa");
		}
		softAssert.assertNotEquals(aCreateNew.slugInput.getAttribute("value").length(), properties.getProperty("oneHundredTwenty"));
		softAssert.assertEquals(aCreateNew.slugInput.getAttribute("value").length(), properties.getProperty("oneHundred"));
	}
	
	//TODO: S&C - b/c no longer have form filled completely out
	@Test (enabled = false)
	public void testSlugEmpty() {
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.slugInput.clear();
		aCreateNew.saveAsDraftButton.click();
		//should not be left empty
		//Assert.assertNotEquals(aCreateNew.slugInput.getText(), "");
	}
	
	
	@Test (enabled = false)
	public void testMetaTagsToTags() {
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.tagsDivInput.clear();
		aCreateNew.tagsInput.sendKeys(properties.getProperty("RANKtag"));
		aCreateNew.tagsInput.sendKeys(Keys.RETURN);
		aCreateNew.metaKeywordsInput.sendKeys(properties.getProperty("selectOption"));
		aCreateNew.metaKeywordsInput.sendKeys(Keys.RETURN);
		aCreateNew.tagsInput.sendKeys(properties.getProperty("lettersTest"));
	}
	
	
	
	

	@BeforeMethod
	public void beforeMethod() {
		//need if statement to determine what page we are on - click create new button
//		aCreateNew = new AssessmentsCreateNew(driver);
//		//fill in mandatory fields with what should be acceptable input
//		//activity points and pass % have default values
//		aCreateNew.assessmentNameInput.clear();
//		aCreateNew.assessmentNameInput.sendKeys(properties.getProperty("alphaNumUnderscoreTitle"));
//		aCreateNew.tagsInput.clear();
//		aCreateNew.tagsInput.sendKeys(properties.getProperty("JTAtag"));
//		aCreateNew.tagsInput.sendKeys(Keys.RETURN);
//		aCreateNew.tagsInput.sendKeys(properties.getProperty("ACNtag"));
//		aCreateNew.tagsInput.sendKeys(Keys.RETURN);
//		aCreateNew.tagsInput.sendKeys(properties.getProperty("RANKtag"));
//		aCreateNew.tagsInput.sendKeys(Keys.RETURN);
//		aCreateNew.slugInput.clear();
//		aCreateNew.slugInput.sendKeys(properties.getProperty("alphaNumUnderscoreTitle"));
//		Select levelDropdown = new Select(aCreateNew.levelDropdown);
//		levelDropdown.selectByValue(properties.getProperty("level100value"));
//		aCreateNew.categoryDropdown.click();
//		aCreateNew.dotNetCategory.click();
//		aCreateNew.container.click();
		
	}

	@AfterMethod
	public void afterMethod() {
		
		//TODO: need to make sure you are still on the Assessments Create New Page
	}

	@DataProvider
	public Object[][] dp() {
		return new Object[][] { new Object[] { 1, "a" }, new Object[] { 2, "b" }, };
	}

	@BeforeClass 
	public void beforeClass() throws IOException {
		//Navigate to Assessment Create New Page
		
		File configFile = new File("src/main/resources/config.properties");
		FileInputStream fileInput = new FileInputStream(configFile);
		properties.load(fileInput);
		fileInput.close();

		//TODO: this needs to be a relative path.
		File file = new File("C:\\Automation\\chromedriver_win32\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://preview.revature.com/core/");

		LoginPage loginPage = new LoginPage(driver);
		loginPage.username.sendKeys(properties.getProperty("username"));
		loginPage.password.sendKeys(properties.getProperty("password"));
		loginPage.submit.click();
		while (driver.getTitle().equals("Revature | Admin")) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		System.out.println(driver.getTitle());
		
		//TODO: may need to add in implicit wait loops for 
		// each page change
		HomePage hPage = new HomePage(driver);
		hPage.AssessmentsLink.click();
		AssessmentsPage aPage = new AssessmentsPage(driver);
		aPage.createNewButton.click();
	}

	@AfterClass
	public void afterClass() {
		HomePage hPage = new HomePage(driver);
		hPage.settingsDropdown.click();
		hPage.logOutButton.click();
		driver.quit();
	}

	@BeforeTest
	public void beforeTest() {
		
	}

	@AfterTest
	public void afterTest() {
	}

	@BeforeSuite
	public void beforeSuite() {
	}

	@AfterSuite
	public void afterSuite() {
	}

}
