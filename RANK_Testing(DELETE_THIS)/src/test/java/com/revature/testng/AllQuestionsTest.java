package com.revature.testng;

import org.testng.annotations.Test;

import com.revature.factory.AllQuestions;
import com.revature.factory.HomePage;
import com.revature.factory.LoginPage;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class AllQuestionsTest {
	Properties properties = new Properties();
	WebDriver driver;

	@Test(priority = 1)
	public void basicSearchTest() {
		AllQuestions aQPage = new AllQuestions(driver);
		/*
		 * get the text of the tags before searching in order to wait for the search to
		 * load note: this needs to be changed because if the search returns the same
		 * first question the while loop won't end
		 */
		String originalText = aQPage.Tags.getText();

		// Send a search query
		aQPage.Search.sendKeys(properties.getProperty("searchQuery"));
		aQPage.Search.sendKeys(Keys.RETURN);
		/*
		 * This loop will make multiple two attempts to access the DOM elements this
		 * should protect against JavaScript updating the DOM while trying to access the
		 * elements
		 */
		int attempts = 0;
		while (attempts < 2) {
			try {
				// While the first Tag is still the same, wait for the page to load
				while (aQPage.Tags.getText().equals(originalText)) {
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				}
			} catch (StaleElementReferenceException e) {
				e.printStackTrace();
			}
			attempts++;
		}

		// Assert that the search query is within the Tags
		Assert.assertTrue(
				aQPage.Tags.getText().toUpperCase().contains(properties.getProperty("searchQuery").toUpperCase()));
	}

	@Test(priority = 2)
	public void resetTest() {
		AllQuestions aQPage = new AllQuestions(driver);
		String tags = aQPage.Tags.getText();
		aQPage.Reset.click();
		int attempts = 0;
		while (attempts < 2) {
			try {
				while (aQPage.Tags.getText().equals(tags)) {
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				}
			} catch (StaleElementReferenceException e) {
				e.printStackTrace();
			}
			attempts++;
		}
		Assert.assertEquals(aQPage.Search.getAttribute("value"), "");
	}

	@Test(priority = 3)
	public void displayDropdownTest() {
		AllQuestions aQPage = new AllQuestions(driver);
		aQPage.DisplayDropdown.sendKeys(Keys.ARROW_DOWN);
		while (aQPage.rows.size() == 10) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		Assert.assertEquals(aQPage.rows.size(), 20);
	}

	@Test(priority = 4)
	public void viewQuestionTest() {
		AllQuestions aQPage = new AllQuestions(driver);
		aQPage.QuestionsSelection.click();
		while (!aQPage.closeQuestionButton.isDisplayed()) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		aQPage.closeQuestionButton.click();
		while (aQPage.closeQuestionButton.isDisplayed()) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}

	// Test disabled because we are not supposed to delete questions from the
	// database
	@Test(enabled = false)
	public void deleteQuestionTest() {
		AllQuestions aQPage = new AllQuestions(driver);
		aQPage.Search.clear();
		aQPage.Search.sendKeys(properties.getProperty("teamNameSearch"));
		aQPage.Search.sendKeys(Keys.RETURN);
		while (aQPage.rows.size() == 10) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		aQPage.deleteQuestionButton.click();
	}

	@Test(priority = 6)
	public void editQuestionTest() {
		AllQuestions aQPage = new AllQuestions(driver);
		while (!aQPage.editQuestionButton.isDisplayed()) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		aQPage.editQuestionButton.click();
	}

	@Test(dataProvider = "dp")
	public void f(Integer n, String s) {
	}

	@BeforeMethod
	public void beforeMethod() {
	}

	@AfterMethod
	public void afterMethod() {
	}

	@DataProvider
	public Object[][] dp() {
		return new Object[][] { new Object[] { 1, "a" }, new Object[] { 2, "b" }, };
	}

	@BeforeClass
	public void beforeClass() throws IOException {
		File configFile = new File("src/main/resources/config.properties");
		FileInputStream fileInput = new FileInputStream(configFile);
		properties.load(fileInput);
		fileInput.close();

		File file = new File("C:\\Automation\\chromedriver_win32\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://preview.revature.com/core/");
		LoginPage loginPage = new LoginPage(driver);
		loginPage.username.clear();
		loginPage.password.clear();
		loginPage.username.sendKeys(properties.getProperty("username"));
		loginPage.password.sendKeys(properties.getProperty("password"));
		loginPage.submit.click();
		while (driver.getTitle().equals(properties.getProperty("loginPageTitle"))) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		HomePage hPage = new HomePage(driver);
		hPage.QuestionsDropdown.click();
		hPage.AllQuestionsLink.click();
		while (driver.getTitle().equals(properties.getProperty("homePageTitle"))) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

	@BeforeTest
	public void beforeTest() {
	}

	@AfterTest
	public void afterTest() {
	}

	@BeforeSuite
	public void beforeSuite() {
	}

	@AfterSuite
	public void afterSuite() {
	}

}
