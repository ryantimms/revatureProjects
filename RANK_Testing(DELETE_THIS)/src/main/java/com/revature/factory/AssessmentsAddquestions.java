package com.revature.factory;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AssessmentsAddquestions {
	public AssessmentsAddquestions(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:atchQsnLinkId\"]")
	public WebElement AddQuestion;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:poolId\"]")
	public WebElement PoolName;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtUnAsignQsnTblId_length\"]/label/select")
	public WebElement AssessmentDisplayDropdown;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtUnAsignQsnTblId\"]/tbody/tr[1]/td[1]/input")
	public WebElement CheckBox;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:msgId\"]/li")
	public WebElement alerttag;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtUnAsignQsnTblId\"]/thead/tr/th[1]/input")
	public WebElement ParentCheckBox;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:j_idt940:0:asmtQsnTblId\"]/tbody/tr/td[2]/input")
	public WebElement StickyCheckBox;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:tempUnAssignQsnsPan\"]/fieldset/legend")
	public WebElement StickyAddedQuestionsTag;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:j_idt940:0:delQsnBtn\"]")
	public WebElement DeleteQuestion;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:pushToTmpListId\"]")
	public WebElement AddToList;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:qsnViewPanId\"]/div[2]/div/div/div/div/div[1]/div[1]/span")
	public WebElement QuestionType;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:attach-panel-back-btn\"]")
	public WebElement Back;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:j_idt626:0:tempAsignQsnTblId:0:qsnViewIdUnAsignQsn\"]")
	public WebElement Questionlinkclick;

	@FindBy(xpath = "//*[@id=\"cancel-BTN\"]/i")
	public WebElement close;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtUnAsignQsnTblId\"]/tbody/tr")
	public List<WebElement> rows;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:attach-question-btn\"]")
	public WebElement AttachtoAssessment;
}
