package com.revature.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.revature.model.User;
import com.revature.util.ConnectionUtil;
import com.revature.util.LogUtil;
//import com.revature.util.LogUtil;

public class UserDaoImp implements UserDao {
	// Singleton implementation
	private UserDaoImp() {

	}

	private static UserDaoImp userDaoImp;

	public static UserDaoImp getInstance() {
		if (userDaoImp == null) {
			userDaoImp = new UserDaoImp();
		}
		return userDaoImp;
	}

	@Override
	public boolean insert(User user) {
		try (Connection conn = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			String command = "INSERT INTO Users VALUES(NULL,?,?,?,?,?,?)";

			PreparedStatement statement = conn.prepareStatement(command);

			// Set attributes to be inserted
			statement.setString(statementIndex, user.getUserName().toLowerCase());
			statement.setString(++statementIndex, user.getPassword());
			statement.setString(++statementIndex, user.getFirstName().toUpperCase());
			statement.setString(++statementIndex, user.getLastName().toUpperCase());
			statement.setString(++statementIndex, user.getEmail().toLowerCase());
			statement.setInt(++statementIndex, user.getRole_id());
			//Executes the SQL command and returns the row count if successful, 0 if not
			if (statement.executeUpdate() > 0) {
				return true;
			}
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception inserting new user.", e);
		}
		return false;
	}
		//Inserts a new user via callableStatement for more security, using stored procedure
	@Override
	public boolean insertProc(User user) {
		try (Connection conn = ConnectionUtil.getConnection()) {

			int statementIndex = 0;
			String command = "{CALL ADDUSER(?,?,?,?,?,?)}";

			CallableStatement statement = conn.prepareCall(command);

			// Set attributes to be inserted
			statement.setString(++statementIndex, user.getUserName().toLowerCase());
			statement.setString(++statementIndex, user.getPassword());
			statement.setString(++statementIndex, user.getFirstName().toUpperCase());
			statement.setString(++statementIndex, user.getLastName().toUpperCase());
			statement.setString(++statementIndex, user.getEmail().toLowerCase());
			statement.setInt(++statementIndex, user.getRole_id());

			
			
			//Executes the SQL command and returns the row count if successful, 0 if not
			if (statement.executeUpdate() >0) {
				return true;
			}
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception inserting new user.", e);
		}
		return false;
	}
//TODO IMPLEMENT PASSWORD UPDATE
	@Override
	public boolean update(User user) {
		try(Connection conn=ConnectionUtil.getConnection()){
			
			/*Gets an instance of the User class based on the UserID which the user cannot change
			 *This gets the user information stored from the database as opposed to the user object being
			 *passed to the update() method, which has the new information in it
			 */
			User loggedUser= UserDaoImp.getInstance().getUser(user);
			String command="";
				int statementIndex=0;
				command="UPDATE USERS SET USER_EMAIL=? WHERE USER_ID=?";
				PreparedStatement statement=conn.prepareStatement(command);
				statement.setString(++statementIndex, loggedUser.getEmail());
				statement.setInt(++statementIndex, user.getUser_id());
				statementIndex=0;
				command="UPDATE USERS SET USER_FIRSTNAME=? WHERE USER_ID=?";
				PreparedStatement statement2=conn.prepareStatement(command);
				statement2.setString(++statementIndex, loggedUser.getFirstName());
				statement2.setInt(++statementIndex, user.getUser_id());
				statementIndex=0;
				command="UPDATE USERS SET USER_LASTNAME=? WHERE USER_ID=?";
				PreparedStatement statement3=conn.prepareStatement(command);
				statement3.setString(++statementIndex, loggedUser.getLastName());
				statement3.setInt(++statementIndex, user.getUser_id());
				
				if((statement.executeUpdate()>0)&&(statement2.executeUpdate()>0)&&(statement3.executeUpdate()>0)) {
					return true;
				}
			
		} catch (SQLException e) {
			LogUtil.logger.warn("Exception updating user information.", e);
		}
		return false;
	}

	//SELECT USERS BASED ON THEIR USER_ID
	@Override
	public User getUser(User user) {
		try(Connection conn=ConnectionUtil.getConnection()){
			int StatementIndex=0;
			String command = "SELECT * FROM USERS WHERE USER_ID=?";
			PreparedStatement statement = conn.prepareStatement(command);
			statement.setLong (++StatementIndex,user.getUser_id());
			ResultSet result= statement.executeQuery();
			while(result.next()) {
			return new User(
					result.getInt("USER_ID"),
					result.getInt("USER_ROLE_ID"),
					result.getString("USER_FIRSTNAME"),
					result.getString("USER_LASTNAME"), 
					result.getString("USER_USERNAME"), 
					result.getString("USER_PASSWORD"),
					result.getString("USER_EMAIL")
					);
			}
		} catch(SQLException e) {
			LogUtil.logger.warn("Exception selecting a user.", e);
		}
		
		return null;
	}
	
	
//SELECT USERS BASED ON USER NAME	
	@Override
	public User getUserByUserName(User user) {
		//TODO CONNECTION IS NOT GETTING MADE TO DATABASE????
		try{
			Connection conn=ConnectionUtil.getConnection();
			int StatementIndex=0;
			String command = "SELECT * FROM USERS WHERE USER_USERNAME=?";
			PreparedStatement statement = conn.prepareStatement(command);
			statement.setString (++StatementIndex,user.getUserName());
			ResultSet result= statement.executeQuery();
			while(result.next()) {
			return new User(
					result.getInt("USER_ID"),
					result.getInt("USER_ROLE_ID"),
					result.getString("USER_FIRSTNAME"),
					result.getString("USER_LASTNAME"), 
					result.getString("USER_USERNAME"), 
					result.getString("USER_PASSWORD"),
					result.getString("USER_EMAIL")
					);
			}
			return new User();
		} catch(SQLException e) {
			e.getStackTrace();
			LogUtil.logger.warn("Exception selecting a user by username.", e);
		}
	
		return new User();
	}
	
	
	@Override
	public String getUserHash(User user) {
		try(Connection connection = ConnectionUtil.getConnection()) {
			int statementIndex = 0;
			String command = "SELECT GET_USER_HASH(?,?) AS HASH FROM DUAL";
			PreparedStatement statement = connection.prepareStatement(command);
			statement.setString(++statementIndex, user.getUserName());
			statement.setString(++statementIndex, user.getPassword());
			ResultSet result = statement.executeQuery();
			
			if(result.next()) {
				return result.getString("HASH");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LogUtil.logger.warn("Exception getting user hash.", e);

		}
		return null;
	}

	@Override
	public List<User> getAllEmployees() {
		try(Connection conn=ConnectionUtil.getConnection()){
			List<User> allEmployees=new ArrayList<>();
			String command = "SELECT * FROM USERS WHERE USER_ROLE_ID=1";
			PreparedStatement statement = conn.prepareStatement(command);
			ResultSet result= statement.executeQuery();
			while(result.next()) {
				allEmployees.add(new User(result.getInt("USER_ID"),
										  result.getInt("USER_ROLE_ID"),
										  result.getString("USER_FIRSTNAME"),
										  result.getString("USER_LASTNAME"),
										  result.getString("USER_USERNAME"),
										  result.getString("USER_PASSWORD"),
										  result.getString("USER_EMAIL")
						));
			}
			return allEmployees;
		} catch(SQLException e) {
			LogUtil.logger.warn("Exception selecting all users.", e);
		}		return null;
	}
}
