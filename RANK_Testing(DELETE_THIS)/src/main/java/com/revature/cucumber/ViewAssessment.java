package com.revature.cucumber;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import com.revature.factory.AssessmentsPage;
import com.revature.factory.HomePage;
import com.revature.factory.LoginPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ViewAssessment {
	Properties properties = new Properties();
	public static WebDriver driver;
	void setUp() throws IOException {
		File configFile = new File("src/main/resources/config.properties");
		FileInputStream fileInput = new FileInputStream(configFile);
		properties.load(fileInput);
		fileInput.close();

		File file = new File("C:\\Automation\\chromedriver_win32\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://preview.revature.com/core/");
	}
	
	@Given("^I am logged in to an Admin account$")
	public void i_am_logged_in_to_an_Admin_account() throws Throwable {
		setUp();
		LoginPage login = new LoginPage(driver);
		login.username.sendKeys(properties.getProperty("username"));
		login.password.sendKeys(properties.getProperty("password"));
		login.submit.click();
		while(driver.getTitle().equals(properties.getProperty("loginPageTitle"))) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}

	@Given("^I am able to view the main assessment page$")
	public void i_am_able_to_view_the_main_assessment_page() throws Throwable {
		HomePage home = new HomePage(driver);
		home.AssessmentsLink.click();
	}

	@When("^I type a search query into the search bar$")
	public void i_type_a_search_query_into_the_search_bar() throws Throwable {
		AssessmentsPage aPage = new AssessmentsPage(driver);
		aPage.searchBar.sendKeys(properties.getProperty("searchQuery"));
	}

	@When("^I click on one of the results$")
	public void i_click_on_one_of_the_results() throws Throwable {
		AssessmentsPage aPage = new AssessmentsPage(driver);
		aPage.assessmentNameLink.click();
	}

	@Then("^I am able to view the assessment I clicked on$")
	public void i_am_able_to_view_the_assessment_I_clicked_on() throws Throwable {
		Assert.assertNotEquals(driver.getCurrentUrl(), properties.getProperty("assessmentHomeURL"));
		driver.quit();
	}
}
