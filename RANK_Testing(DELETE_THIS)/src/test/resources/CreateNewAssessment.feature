#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios 
#<> (placeholder)
#""
## (Comments)

#Sample Feature Definition Template
@tag
Feature: Create New Assessment
	As a trainer
	I want to create an assessment
	and edit the fields.
	In order to test my associates.

Background:
Given I am on the Create New Assessment Page
	And I have filled in the mandatory fields

@CreatingAssessment
Scenario: Creating Basic Assessment 
When I click save as draft
Then I should see my assessment in the Assessments table

@SavingAndContinuing
Scenario: Making Changes in an Assessment After Saving
When I click Save and Continue
	And change the text in a field
	And click Save and Continue again
Then my changes should be visible in the form
