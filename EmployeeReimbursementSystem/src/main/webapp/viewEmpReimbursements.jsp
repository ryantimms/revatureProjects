<%@page import="com.revature.model.Reimbursement"%>
<%@page import="com.revature.controller.ViewEmpReimbursementsController" %>
<%@page import="com.revature.model.User" %>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View all Reimbursements</title>
</head>
<body>
	<%List<Reimbursement> allReimbs = ViewEmpReimbursementsController.showAll((User) request.getSession().getAttribute("loggedUser")); %>
	<% for(Reimbursement r : allReimbs){
	%>
	<table>
	<thead>
	<tr>
		<th>Amount</th>
		<th>Description</th>
		<th>Author ID</th>
		<th>Resolver ID(43 if pending)</th>
		<th>Type(1 for Travel, 2 for Certification, 3 for Food)</th>
		<th>Status(1 for Approved, 2 for Denied, 3 for Pending)</th>
	</thead>
	<tr>
	<td><%=r.getReimb_amount() %></td>
	<td><%=r.getReimb_description() %></td>
	<td><%=r.getReimb_author() %></td>
	<td><%=r.getReimb_resolver() %></td>
	<td><%=r.getReimb_type_id() %></td>
	<td><%=r.getReimb_status_id() %></td>
	</tr>
	</table>
	<% } %>
</body>
</html>