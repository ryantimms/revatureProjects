package com.revature.request;

import javax.servlet.http.HttpServletRequest;

import com.revature.controller.LogOutController;
import com.revature.controller.LoginController;
import com.revature.controller.NewReimbursementController;
import com.revature.controller.RegisterController;
import com.revature.controller.ResolveController;
import com.revature.controller.SetSearchController;
import com.revature.controller.UpdateController;

public class RequestHelper {
	public static String process(HttpServletRequest request) {
		switch (request.getRequestURI()) {
		case "/EmployeeReimbursementSystem/Login.do":
			return LoginController.login(request);
		case "/EmployeeReimbursementSystem/newUser.do":
			return RegisterController.registerNewUser(request);
		case "/EmployeeReimbursementSystem/LogOut.do":
			System.out.println(request.getSession().getAttribute("loggedUser"));
			return LogOutController.logout(request);
		case "/EmployeeReimbursementSystem/updateInfo.do":
			return UpdateController.update(request);
		case "/EmployeeReimbursementSystem/newReimbursement.do":
			return NewReimbursementController.submitNewTicket(request);
		case "/EmployeeReimbursementSystem/setSearchID.do":
			return SetSearchController.setup(request);
		case "/EmployeeReimbursementSystem/resolve.do":
			return ResolveController.resolveReimb(request);
		/*
		 * TODO ADD OTHER PAGE REQUESTS
		 */
		}
		return "index.html";
	}
}
