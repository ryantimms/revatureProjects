package com.revature.factory;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AllQuestions_CreateNew {

	public AllQuestions_CreateNew(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//*[@id=\"qsnMgmtForm:type-id\"]")
	public WebElement questionType;

	@FindBy(id = "qsnMgmtForm:titleId")
	public WebElement questionTitle;

	// may need to go out to the html element to select this @kp
	@FindBy(xpath = "//*[@id=\"cke_252_contents\"]")
	public WebElement questionContent;

	@FindBy(id = "qsnMgmtForm:catId")
	public WebElement category;

	@FindBy(id = "qsnMgmtForm:lvlId")
	public WebElement level;

	@FindBy(id = "qsnMgmtForm:pointId")
	public WebElement skillPoints;

	@FindBy(id = "qsnMgmtForm:scoreId")
	public WebElement score;

	@FindBy(id = "qsnMgmtForm:durId")
	public WebElement duration;
	
	@FindBy(xpath="//*[@id=\"eIntern-admin-UI-body\"]/div[3]/table/tbody/tr[1]/td[1]/a")
	public WebElement incrementHour;
	
	@FindBy(xpath="//*[@id=\"eIntern-admin-UI-body\"]/div[3]/table/tbody/tr[3]/td[1]/a")
	public WebElement decrementHour;
	
	@FindBy(xpath="//*[@id=\"eIntern-admin-UI-body\"]/div[3]/table/tbody/tr[1]/td[3]/a")
	public WebElement incrementMinute;
	
	@FindBy(xpath="//*[@id=\"eIntern-admin-UI-body\"]/div[3]/table/tbody/tr[3]/td[3]/a")
	public WebElement decrementMinute;
	
	@FindBy(xpath="//*[@id=\"eIntern-admin-UI-body\"]/div[3]/table/tbody/tr[1]/td[5]/a")
	public WebElement incrementSecond;
	
	@FindBy(xpath="//*[@id=\"eIntern-admin-UI-body\"]/div[3]/table/tbody/tr[3]/td[5]/a")
	public WebElement decrementSecond;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:qsnAddEditPanId\"]/div/div[9]/div/div[1]/div/span/div")
	public WebElement tagsDiv;
	
	@FindBy(xpath = "//*[@id=\"qsnMgmtForm:qsnAddEditPanId\"]" + "/div/div[9]/div/div[1]/div/span/div/input[2]")
	public WebElement tags;

	@FindBy(xpath = "//*[@id=\"qsnMgmtForm:saveLink\"]")
	public WebElement saveButton;

	@FindBy(xpath = "//*[@id=\"qsnMgmtForm\"]/div/div[1]/div[2]/a")
	public WebElement backButton;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:isCorBoolCheckBoxId:0\"]")
	public WebElement trueRadio;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:isCorBoolCheckBoxId:1\"]")
	public WebElement falseRadio;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:optionsPanId\"]/div[3]/div[1]/textarea")
	public WebElement trueExplanation;

	@FindBy(xpath="//*[@id=\"qsnMgmtForm:optionsPanId\"]/div[4]/div[1]/textarea")
	public WebElement falseExplanation;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:qsnOptTblId\"]")
	public WebElement bestChoiceTable;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:optionTxtForNu\"]")
	public WebElement numericalAnswer;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:addQsnAns\"]")
	public WebElement mChoiceAddAnswerButton;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:qsnOptTblId:0:option-del-btn\"]")
	public WebElement mChoiceDeleteAnswerButton;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:qsnOptTblId:0:j_idt324\"]")
	public WebElement mChoiceYesCheckbox;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:qsnOptTblId:0:weightageId\"]")
	public WebElement mChoicePercentTextbox;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:qsnOptTblId:0:weightageIdPan\"]/div[2]/div[1]/div/div/button[1]")
	public WebElement mChoicePercentScrollUpBtn;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:qsnOptTblId:0:weightageIdPan\"]/div[2]/div[1]/div/div/button[2]")
	public WebElement mChoicePercentScrollDownBtn;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:qsnOptTblId\"]/tbody/tr")
	public List<WebElement> rows;
	
	@FindBy(xpath="//*[@id=\"qsnMgmtForm:qsnOptTblId\"]/tbody/tr[1]/td[2]/div[3]/textarea")
	public WebElement mChoiceExp;

}