package com.revature.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "loginForm:userName-input-id")
	public WebElement username;
	@FindBy(id = "loginForm:input-psw")
	public WebElement password;
	@FindBy(id = "loginForm:login-btn-id")
	public WebElement submit;
	@FindBy(id = "qsnMgmtForm:tblselectmenu")
	public WebElement dropdown;
	@FindBy(id="asmtMgmtForm:lvl-ovr-id:0")
	public WebElement OverrideRadioOn;
	@FindBy(id="asmtMgmtForm:lvl-ovr-id:1")
	public WebElement OverrideRadioOff;
}
