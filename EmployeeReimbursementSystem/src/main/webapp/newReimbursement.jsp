<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Login page">
<meta name="author" content="Ryan">

<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<link href="background.css" rel="stylesheet">

<title>New User Registration</title>
</head>
<body>
	<nav class="navbar">
	<div class="navbar navbar-inverse fixed-top">
		<div class="navbar-header">
			<a class="navbar-brand">Revature</a>
		</div>
			<ul class="nav navbar-nav">
				<li><a href="home.html">Home</a></li>

				<li><a href="EmpReimbursementHome.jsp">Reimbursements</a></li>

				<li><a href="viewProfile.jsp">View Profile</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/EmployeeReimbursementSystem/LogOut.do">Log Out</a></li>
			</ul>
	</div>

</nav>
	
	<div class="container">
		<form method="POST" action="/EmployeeReimbursementSystem/newReimbursement.do">
			<div class="form-group">
				<label for="amount">Reimbursement Amount:</label> <input type="text"
					class="form-control" name="amount" placeholder="Amount">
			</div>
			<div class="form-group">
				<label for="description">Description:</label> <input type="text"
					class="form-control" name=description placeholder="Description">
			</div>
			<label for="sel1">Reimbursement type(1 for Travel, 2 for Certification, 3 for Food):</label> <input
				type="text" class="form-control" name="type" placeholder="1"/> <br> <br>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
		</form>
	</div>
</body>
</html>