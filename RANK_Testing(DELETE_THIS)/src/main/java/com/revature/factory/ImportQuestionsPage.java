package com.revature.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ImportQuestionsPage {
	
	public ImportQuestionsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//*[@id=\"formatForm:file-format:0\"]")
	public WebElement AikenFormatRadio;
	
	@FindBy(xpath="//*[@id=\"formatForm:file-format:1\"]")
	public WebElement XMLFormatRadio;
	
	@FindBy(xpath="//*[@id=\"formatForm:catId\"]")
	public WebElement categoryDropdown; //*[@id="s2id_formatForm:catId"]
	
	@FindBy(id="formatForm:catId")
    public WebElement levelDropdown; 
	
	@FindBy(xpath="//*[@id=\"formatForm:pointId\"]")
	public WebElement skillPointsInput;
	
	@FindBy(xpath="//*[@id=\"formatForm:scoreId\"]")
	public WebElement scoreInput;
	
	@FindBy(xpath="//*[@id=\"formatForm:durId\"]")
	public WebElement durationInput;
	
	@FindBy(xpath="//*[@id=\"eIntern-admin-UI-body\"]/div[6]")
	public WebElement timePicker;
	
	@FindBy(xpath="//*[@id=\"eIntern-admin-UI-body\"]/div[6]/table/tbody/tr[1]/td[1]/a")
	public WebElement incHour;
	
	@FindBy(xpath="//*[@id=\"eIntern-admin-UI-body\"]/div[6]/table/tbody/tr[1]/td[3]/a")
	public WebElement incMinute; 
	
	@FindBy (xpath="//*[@id=\"eIntern-admin-UI-body\"]/div[6]/table/tbody/tr[3]/td[5]/a")
	public WebElement decSecond;
	
	@FindBy(xpath="//*[@id=\"formatForm:initFormater\"]/div/div/div/div[3]/div[7]/div/div[1]/div/span/div/input[2]")
	public WebElement tagsInput;
	
	@FindBy(xpath="//*[@id=\"formatForm:aikenInputId\"]")
	public WebElement chooseFileButtonAiken;
	
	@FindBy (xpath="//*[@id=\"formatForm:aikenLnk\"]")
	public WebElement importButtonAiken;
	
	@FindBy(xpath="//*[@id=\"formatForm:xmlInputId\"]")
	public WebElement chooseFileButtonXML;
	
	@FindBy(xpath="//*[@id=\"formatForm:xmlLink\"]")
	public WebElement importButtonXML;
	
	@FindBy(xpath="//*[@id=\"formatForm:downXmlLink\"]")
	public WebElement sampleXMLDownloadButton;
	
	@FindBy(xpath="//*[@id=\"formatForm:msgId\"]/li")
	public WebElement xmlErrorMsg;
	
	@FindBy(xpath="//*[@id=\"formatForm:msgId\"]/li")
	public WebElement aikenErrorMsg;
	
	@FindBy(xpath="//*[@id=\"formatForm\"]/input[2]")
	public WebElement hiddenInputOnFileChange;
	
	//save new questions after successfully imported
	@FindBy(xpath="//*[@id=\"formatForm:j_idt114\"]")
	public WebElement saveNewQs;
	
	@FindBy(xpath="//*[@id=\"s2id_formatForm:catId\"]")
	public WebElement labelForSelect;
	
	//Import questions menu item, not technically a part of my factory but necessary to finish testing
	@FindBy(xpath="//*[@id=\"importMgmt\"]/a")
	public WebElement importQMenuOption;
	
	//individual tag
	@FindBy(xpath="//*[@id=\"formatForm:initFormater\"]/div/div/div/div[3]/div[7]/div/div[1]/div/span/div/span")
	public WebElement tag;
}
