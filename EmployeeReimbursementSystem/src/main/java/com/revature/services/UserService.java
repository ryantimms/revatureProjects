package com.revature.services;

import com.revature.dao.UserDaoImp;
import com.revature.model.User;

public class UserService {
	private static UserService userService;

	// Singleton implementation
	private UserService() {

	}

	public static UserService getInstance() {
		if (userService == null) {
			userService = new UserService();
		}

		return userService;
	}

	// Calls the insert method of the DAO
	public boolean registerUser(User user) {
		return UserDaoImp.getInstance().insert(user);
	}

	// Calls the insert by procedure method of the DAO
	public boolean registerUserProc(User user) {
		return UserDaoImp.getInstance().insertProc(user);
	}

	// Calls the update method of the DAO
	public boolean updateUser(User user) {
		return UserDaoImp.getInstance().update(user);
	}

	// Login method uses the getUser method then compares the passwords to check for
	// login potential
	public User login(User user) {

		// Uses the getUser method to SELECT the non-validated user from the database
		System.out.println(UserDaoImp.getInstance().getUserByUserName(user));
		
		User loggedUser = UserDaoImp.getInstance().getUserByUserName(user);
		if (loggedUser.getPassword().equals(UserDaoImp.getInstance().getUserHash(user))) {
			return loggedUser;
		}

		return new User();
	}

	public boolean isUserNameTaken(User user) {
		// Search database by user name, if the user name does not match any others in
		// the DB then false is returned
		if (UserDaoImp.getInstance().getUserByUserName(user).getUser_id()==0) {
			return false;
		}
		return true;
	}
}
