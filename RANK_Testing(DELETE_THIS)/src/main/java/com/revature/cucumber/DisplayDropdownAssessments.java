package com.revature.cucumber;

import org.testng.Assert;

import com.revature.factory.AssessmentsPage;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DisplayDropdownAssessments {

	@When("^I click a (\\d+) from the display dropdown$")
	public void i_click_a_from_the_display_dropdown(int arg1) throws Throwable {
		AssessmentsPage aPage= new AssessmentsPage(ViewAssessment.driver);
		String displayParam = String.valueOf(arg1);
		aPage.displayDropDown.sendKeys(displayParam);
	}

	@Then("^The number of (\\d+) in the table is the same as the number chosen$")
	public void the_number_of_in_the_table_is_the_same_as_the_number_chosen(int arg1) throws Throwable {
		AssessmentsPage aPage= new AssessmentsPage(ViewAssessment.driver);
		Assert.assertTrue(aPage.rows.size()>arg1);
		ViewAssessment.driver.quit();
	}
}
