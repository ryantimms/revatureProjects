package com.revature.testng;

import org.testng.annotations.Test;



import com.revature.factory.AssessmentsAddquestions;
import com.revature.factory.AssessmentsCreateNew;
import com.revature.factory.AssessmentsPage;
import com.revature.factory.LoginPage;
import com.revature.factory.HomePage;


import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeMethod;


public class AddQuestionsTest {
	Properties properties = new Properties();
	WebDriver driver;

	@AfterMethod
	public void afterMethod() {
	}

	@DataProvider
	public Object[][] dp() {
		return new Object[][] { new Object[] { 1, "a" }, new Object[] { 2, "b" }, };
	}

	@BeforeClass
	public void beforeClass() throws IOException {

		File configFile = new File("src/main/resources/config.properties");
		FileInputStream fileInput = new FileInputStream(configFile);
		properties.load(fileInput);
		fileInput.close();
		File file = new File("C:\\Automation\\chromedriver_win32\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://preview.revature.com/core/");
		LoginPage loginPage = new LoginPage(driver);
		loginPage.username.sendKeys(properties.getProperty("username"));
		loginPage.password.sendKeys(properties.getProperty("password"));
		loginPage.submit.click();
		while (driver.getTitle().equals("Revature | Admin")) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		System.out.println(driver.getTitle());
		Assert.assertNotEquals(driver.getTitle(), "Revature | Admin");

		// fill in mandatory fields with what should be acceptable input
		// activity points and pass % have default values
		HomePage hm = new HomePage(driver);
		hm.AssessmentsLink.click();
		AssessmentsPage ap = new AssessmentsPage(driver);
		ap.createNewButton.click();
		AssessmentsCreateNew aCreateNew = new AssessmentsCreateNew(driver);

		aCreateNew.assessmentNameInput.clear();
		aCreateNew.assessmentNameInput.sendKeys(properties.getProperty("alphaTitle"));
		aCreateNew.tagsInput.clear();
		aCreateNew.tagsInput.sendKeys(properties.getProperty("JTA"));
		aCreateNew.tagsInput.sendKeys(Keys.RETURN);
		aCreateNew.tagsInput.sendKeys(properties.getProperty("RANK"));
		aCreateNew.tagsInput.sendKeys(Keys.RETURN);
		aCreateNew.activityPointsInput.clear();
		aCreateNew.activityPointsInput.sendKeys(properties.getProperty("Points"));
		aCreateNew.activityPointsInput.sendKeys(Keys.RETURN);

		aCreateNew.slugInput.clear();
		aCreateNew.slugInput.sendKeys(properties.getProperty("alphaTitle"));
		Select levelDropdowns = new Select(aCreateNew.levelDropdowns);
		levelDropdowns.selectByValue(properties.getProperty("level"));
		aCreateNew.categoryDropdown.click();
		aCreateNew.dotNetCategory.click();
		aCreateNew.levelOverrideOnRadio.click();
		aCreateNew.addQuestionsButton.click();
		// AssessmentsAddquestions aq=new AssessmentsAddquestions(driver);
		/*
		 * while(aq.Back.isDisplayed()==false) {
		 * driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); }
		 */
	}



	@BeforeMethod
	public void beforeMethod() {

	}

	@Test(priority = 1, enabled = false)
	public void Dropdown() {
		AssessmentsAddquestions aq = new AssessmentsAddquestions(driver);
		aq.AssessmentDisplayDropdown.sendKeys(Keys.ARROW_DOWN);
		while (aq.rows.size() == 10) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

		Assert.assertEquals(aq.rows.size(), 20);

		// Select levelDropdown = new Select(aq.AssessmentDisplayDropdown);
		// levelDropdown.selectByValue(properties.getProperty("20dropdown"));
		// aq.CheckBox.click();
	}

	@Test(priority = 2)
	public void AddtoList() {
		AssessmentsAddquestions qa = new AssessmentsAddquestions(driver);
		Assert.assertNotNull("PoolName");

		qa.PoolName.sendKeys(properties.getProperty("poolname"));
		qa.PoolName.sendKeys(Keys.RETURN);
		if (!qa.CheckBox.isSelected()) {
			qa.CheckBox.click();
		}

		Assert.assertTrue(qa.CheckBox.isSelected(), "Checkbox selected");
		qa.AddToList.click();
		Assert.assertEquals(qa.alerttag.getText(), "Question(s) added successfully.");
		Assert.assertTrue(qa.alerttag.isDisplayed());
		System.out.println(qa.alerttag.getText() + "Added to list was successful");
		qa.AttachtoAssessment.click();
		qa.StickyCheckBox.click();
		System.out.println("Sticky check box is checked" + qa.StickyCheckBox.isSelected());
		System.out.println("Delete question button is enabled" + qa.DeleteQuestion.isEnabled());
		qa.AddQuestion.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		System.out.println(qa.StickyAddedQuestionsTag.getText());

		Assert.assertEquals(qa.StickyAddedQuestionsTag.getText(), "Added Question(s)");

		/*
		 * qa.Questionlinkclick.click(); while (qa.close.isDisplayed() == false) {
		 * driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 * 
		 * }
		 */
		/*
		 * qa.close.click(); System.out.println(qa.QuestionType.isDisplayed());
		 * Assert.assertTrue(qa.close.isDisplayed());
		 * System.out.println(qa.close.isDisplayed());
		 * Assert.assertTrue(qa.close.isEnabled()); qa.close.click();
		 */

	}

	@AfterClass
	public void afterClass() {
		HomePage hPage = new HomePage(driver);
		hPage.settingsDropdown.click();
		hPage.logOutButton.click();
		driver.quit();
	}

}
