package com.revature.factory;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AssessmentsPage {

	public AssessmentsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm\"]/div/div[1]/div/a")
	public WebElement createNewButton;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtPubListTblId_length\"]/label/select")
	public WebElement displayDropDown;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtPubListTblId_filter\"]/label/input")
	public WebElement searchBar;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtPubListTblId\"]/tbody/tr[1]/td[2]/a/span")
	public WebElement assessmentNameLink;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtPubListTblId\"]/tbody/tr[1]/td[10]/div/a[1]")
	public WebElement editAssessmentIcon;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtPubListTblId\"]/tbody/tr[1]/td[10]/div/a[2]")
	public WebElement cloneAssessmentIcon;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtPubListTblId:2:delAsmtBtn\"]")
	public WebElement deleteAssessmentIcon;
	
	@FindBy(xpath="//*[@id=\"asmtMgmtForm:asmtPubListTblId\"]/tbody/tr")
	public List<WebElement> rows;

}