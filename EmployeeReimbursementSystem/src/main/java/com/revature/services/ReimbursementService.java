package com.revature.services;

import com.revature.model.Reimbursement;
import com.revature.dao.ReimbursementDaoImp;
public class ReimbursementService {

		private ReimbursementService() {
			
		}
		
		static ReimbursementService reimbursementService;
		
		public static ReimbursementService getInstance() {
			if(reimbursementService==null) {
				return new ReimbursementService();
			}
			return reimbursementService;
		}
		
		public boolean insertReimbProc(Reimbursement reimbursement) {
			return ReimbursementDaoImp.getInstance().insertReimbursementProc(reimbursement);
		}
		public boolean resolve(Reimbursement reimbursement) {
			return ReimbursementDaoImp.getInstance().resolveReimbursement(reimbursement);
		}
	
}
