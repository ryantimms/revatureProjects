package com.revature.dao;

import java.util.List;
import com.revature.model.Reimbursement;
public interface ReimbursementDao {
	

public List<Reimbursement> selectAll();
public List<Reimbursement> selectAllWaiting();
public List<Reimbursement> selectAllResolved();


public List<Reimbursement> selectSingleEmployeeAll(Reimbursement reimbursement);
public List<Reimbursement> selectSingleEmployeeAllWaiting(Reimbursement reimbursement);
public List<Reimbursement> selectSingleEmployeeAllResolved(Reimbursement reimbursement);


public boolean resolveReimbursement(Reimbursement reimbursement);
public boolean insertReimbursementProc(Reimbursement reimbursement);
}
