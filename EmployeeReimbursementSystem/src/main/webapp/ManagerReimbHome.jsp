<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.revature.model.Reimbursement" %>
<%@ page import="java.util.ArrayList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="ersApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<script type="text/javascript" src="angular.min.js"></script>
<script type="text/javascript" src="controller.js"></script>

<title>Reimbursements</title>
</head>
<body>
	<nav class="navbar">
	<div class="navbar navbar-inverse fixed-top">
		<div class="navbar-header">
			<a class="navbar-brand">Revature</a>
		</div>
		<ul class="nav navbar-nav">
			<li><a href="ManagerHome.html">Home</a></li>

			<li><a href="#">Reimbursements</a></li>

			<li><a href="viewManagerProfile.jsp">View Profile</a></li>
			<li><a href="viewEmployees.jsp">View Employees</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="/EmployeeReimbursementSystem/LogOut.do">Log Out</a></li>
		</ul>
	</div>
	</nav>


	<!-- SEARCH -->
	<div ng-controller="managerSearchController">
		<form method="get" action="/EmployeeReimbursementSystem/setSearchID.do">
			<input type="text" name="search" />
			<input type="submit" value="Set I.D. to search by">
		</form>
		<button ng-click="getDataFromServer()">Execute Search</button>

		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Amount</th>
					<th>Description</th>
					<th>Author ID</th>
					<th>Resolver ID(43 if pending)</th>
					<th>Type(1 for Travel, 2 for Certification, 3 for Food)</th>
					<th>Status(1 for Approved, 2 for Denied, 3 for Pending)</th>
			</thead>
			<tr ng-repeat="r in list">
				<td>{{r.reimb_amount | currency}}</td>
				<td>{{r.reimb_description}}</td>
				<td>{{r.reimb_author}}</td>
				<td>{{r.reimb_resolver}}</td>
				<td>{{r.reimb_type_id}}</td>
				<td>{{r.reimb_status_id}}</td>
			</tr>
		</table>
	</div>


	<!-- View Pending Requests TODO: ADD APPROVE/DENY -->
	<div ng-controller="managerViewPendingController" class="container">
		<button ng-click="getDataFromServer()">Click here to view all
			pending reimbursements.</button>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Reimbursement ID</th>
					<th>Amount</th>
					<th>Description</th>
					<th>Author ID</th>
					<th>Resolver ID(43 if pending)</th>
					<th>Type(1 for Travel, 2 for Certification, 3 for Food)</th>
					<th>Status(1 for Approved, 2 for Denied, 3 for Pending)</th>
			</thead>
			<tr ng-repeat="r in list">
				<td>{{r.reimb_id}}</td>
				<td>{{r.reimb_amount | currency}}</td>
				<td>{{r.reimb_description}}</td>
				<td>{{r.reimb_author}}</td>
				<td>{{r.reimb_resolver}}</td>
				<td>{{r.reimb_type_id}}</td>
				<td>{{r.reimb_status_id}}</td>
			</tr>
		</table>
		<!-- JSP Java code used to resolve reimbursements -->
		<% ArrayList<Reimbursement> unresolvedReimbs = (ArrayList<Reimbursement>) session.getAttribute("allPendingReimbs"); %>
		<form method="POST" action="/EmployeeReimbursementSystem/resolve.do">
		<select name="reimbId" required>
			<% for (Reimbursement r : unresolvedReimbs) 
			{
				out.println("<option value=\"" + r.getReimb_id() + "\">" +r.getReimb_id() + "</option>");
			}%>
		</select>
		<select name="statusId" required>
			<option value="1">Approve</option>
			<option value="2">Deny</option>
		</select> 
		<input type="submit" value="Resolve Reimbursement" /><br><br>
	</form>
	</div>

	<div ng-controller="managerViewResolvedController" class="container">
		<button ng-click="getDataFromServer()">Click here to view all
			resolved reimbursements.</button>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Amount</th>
					<th>Description</th>
					<th>Author ID</th>
					<th>Resolver ID(43 if pending)</th>
					<th>Type(1 for Travel, 2 for Certification, 3 for Food)</th>
					<th>Status(1 for Approved, 2 for Denied, 3 for Pending)</th>
			</thead>
			<tr ng-repeat="r in list">
				<td>{{r.reimb_amount | currency}}</td>
				<td>{{r.reimb_description}}</td>
				<td>{{r.reimb_author}}</td>
				<td>{{r.reimb_resolver}}</td>
				<td>{{r.reimb_type_id}}</td>
				<td>{{r.reimb_status_id}}</td>
			</tr>
		</table>
	</div>
</body>
</html>