package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

import com.revature.model.User;

public class SetSearchController {
public static String setup(HttpServletRequest request) {
	String searchID = request.getParameter("search");
	int userID = Integer.valueOf(searchID);
	User searchedUser = new User(userID);
	request.getSession().setAttribute("searchedUser", searchedUser);
	return "ManagerReimbHome.jsp";
}
}
