package com.revature.testng;

import org.testng.annotations.Test;

import com.revature.factory.LoginPage;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class LoginSmokeTest {
	Properties properties = new Properties();
	WebDriver driver;

	@Test(priority=4)
	public void basicLoginTest() {
		LoginPage loginPage = new LoginPage(driver);
		driver.navigate().refresh();
		loginPage.username.clear();
		loginPage.password.clear();
		loginPage.username.sendKeys(properties.getProperty("username"));
		loginPage.password.sendKeys(properties.getProperty("password"));
		loginPage.submit.click();
		while(driver.getTitle().equals(properties.getProperty("loginPageTitle"))) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		//Assert that the page title changes after logging in (i.e. a successful login)
		Assert.assertNotEquals(driver.getTitle(), properties.getProperty("loginPageTitle"));
	}

	@Test(priority=1)
	public void spacedUserNameTest() {
		LoginPage loginPage = new LoginPage(driver);
		driver.navigate().refresh();
		loginPage.username.clear();
		loginPage.password.clear();
		loginPage.username.sendKeys(properties.getProperty("spacedUsername"));
		loginPage.password.sendKeys(properties.getProperty("password"));
		loginPage.submit.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		Assert.assertEquals(driver.getTitle(), properties.getProperty("loginPageTitle"));
	}
	
	@Test(priority=2)
	public void incorrectUserNameTest() {
		LoginPage loginPage = new LoginPage(driver);
		driver.navigate().refresh();
		loginPage.username.clear();
		loginPage.password.clear();
		loginPage.username.sendKeys(properties.getProperty("incorrectUsername"));
		loginPage.password.sendKeys(properties.getProperty("password"));
		loginPage.submit.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		Assert.assertEquals(driver.getTitle(), properties.getProperty("loginPageTitle"));
	}
	
	@Test(priority=3)
	public void spacedPasswordTest() {
		LoginPage loginPage = new LoginPage(driver);
		driver.navigate().refresh();
		loginPage.username.clear();
		loginPage.password.clear();
		loginPage.username.sendKeys(properties.getProperty("username"));
		loginPage.password.sendKeys(properties.getProperty("spacedPassword"));
		loginPage.submit.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		Assert.assertEquals(driver.getTitle(), properties.getProperty("loginPageTitle"));
	}
	@BeforeMethod
	public void beforeMethod() {
	}

	@AfterMethod
	public void afterMethod() {
	}

	@DataProvider
	public Object[][] dp() {
		return new Object[][] { new Object[] { 1, "a" }, new Object[] { 2, "b" }, };
	}

	@BeforeClass
	public void beforeClass() throws IOException {

		File configFile = new File("src/main/resources/config.properties");
		FileInputStream fileInput = new FileInputStream(configFile);
		properties.load(fileInput);
		fileInput.close();

		File file = new File("C:\\Automation\\chromedriver_win32\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://preview.revature.com/core/");

	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

	@BeforeTest
	public void beforeTest() throws IOException {
		
	}

	@AfterTest
	public void afterTest() {
	}

	@BeforeSuite
	public void beforeSuite() {
	}

	@AfterSuite
	public void afterSuite() {
	}

}
