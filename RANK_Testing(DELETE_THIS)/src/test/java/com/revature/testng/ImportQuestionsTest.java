package com.revature.testng;

import java.io.File;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.revature.factory.HomePage;
import com.revature.factory.ImportQuestionsPage;
import com.revature.factory.LoginPage;

public class ImportQuestionsTest {

	Properties properties = new Properties();
	public static WebDriver driver;

	@Test(priority=0)
	public void testAilenRadio() {
		SoftAssert softAssert = new SoftAssert();
		ImportQuestionsPage iq = new ImportQuestionsPage(driver);
		WebDriverWait wait = new WebDriverWait(driver, 8);
   	    wait.until(ExpectedConditions.visibilityOf(iq.AikenFormatRadio));
		iq.AikenFormatRadio.click();
        softAssert.assertEquals(iq.AikenFormatRadio.isSelected(), true);
	}
	
	
	@Test(priority=1)
	public void testSelectCategory() {
		SoftAssert softAssert = new SoftAssert();
		ImportQuestionsPage iq = new ImportQuestionsPage(driver);
		
		testAilenRadio();
		 
  	     while(!iq.categoryDropdown.isEnabled()) {
  	    	 driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
  	    	 if(iq.categoryDropdown.isEnabled()) {
  	  	    	 System.out.println("categorydropdown is displayed.");
  	  	    	Select category = new Select(iq.categoryDropdown);
  	  			System.out.println("HELLLLLLLLLooooooooooooooooo"+iq.categoryDropdown.toString());
  	  			category.selectByIndex(5);
  	  			
  	  			WebElement selectedElement = category.getFirstSelectedOption();
  	  			String selectedOption = selectedElement.getText();
  	  			
  	  			softAssert.assertEquals(selectedOption.contains(properties.getProperty("categorySelect")), true);	
  	  			break;
  	  	     }
  	     }
  	    
		
	}
	
	@Test(priority=2)
	public void skillPointsInput(){
		SoftAssert softAssert = new SoftAssert();
		ImportQuestionsPage iq = new ImportQuestionsPage(driver);
		
		testAilenRadio();
		
		iq.skillPointsInput.sendKeys(properties.getProperty("scoreOrSkillPoints"));
		softAssert.assertEquals(iq.skillPointsInput.getAttribute("value"), properties.getProperty("scoreOrSkillPoints"));
		
	}
	
	@Test(priority=3)
	public void scoreInput() {
		SoftAssert softAssert = new SoftAssert();
		ImportQuestionsPage iq = new ImportQuestionsPage(driver);
		
		testAilenRadio();
		
		iq.scoreInput.sendKeys(properties.getProperty("scoreOrSkillPoints"));
		softAssert.assertEquals(iq.scoreInput.getAttribute("value"), properties.getProperty("scoreOrSkillPoints"));
	}

	@Test(priority=4)
	public void testDuration() {
		SoftAssert softAssert = new SoftAssert();
		ImportQuestionsPage iq = new ImportQuestionsPage(driver);
		
		
		testAilenRadio();
		
		iq.durationInput.click();
		
		while(!iq.timePicker.isDisplayed()) {
			
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			if(iq.timePicker.isEnabled()) {
				iq.incHour.click();
				iq.incMinute.click();
				iq.decSecond.click();
				softAssert.assertEquals(iq.durationInput.getAttribute("value"), "1h 00m 59s");
				break;
			}
			
		}
		
		iq.chooseFileButtonAiken.sendKeys(properties.getProperty("validAiken"));
		iq.importButtonAiken.click();

		softAssert.assertEquals(iq.aikenErrorMsg.getText().contains(properties.getProperty("successMsg")), true);

	}
	
	@Test(priority=5)
	public void testTags(){
		
		SoftAssert softAssert = new SoftAssert();
		ImportQuestionsPage iq = new ImportQuestionsPage(driver);
		
		
		testAilenRadio();
		
		iq.tagsInput.click();
		iq.tagsInput.sendKeys(properties.getProperty("categorySelect"));
		iq.tagsInput.sendKeys(Keys.RETURN);
		// TODO
		// check output for tags
	    softAssert.assertEquals(iq.tag.getText().contains(properties.getProperty("categorySelect")), true);
		
	}
	
	@Test(priority=6)
	public void testFileInput(){
		
		SoftAssert softAssert = new SoftAssert();
		ImportQuestionsPage iq = new ImportQuestionsPage(driver);
		
		
		testAilenRadio();
		iq.chooseFileButtonAiken.sendKeys(properties.getProperty("validAiken"));
		
		System.out.println("***********HELLLLPPPPPPP"+iq.chooseFileButtonAiken.getAttribute("value"));
		
		softAssert.assertEquals(iq.chooseFileButtonAiken.getAttribute("value").contains(properties.getProperty("validAiken")), true);
		
	}

	@Test(priority=7)
	public void testXML() {

		SoftAssert softAssert = new SoftAssert();
		ImportQuestionsPage iq = new ImportQuestionsPage(driver);
		iq.XMLFormatRadio.click();
		iq.chooseFileButtonXML.sendKeys(properties.getProperty("sampleXML"));
		iq.importButtonXML.click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		softAssert.assertEquals(iq.xmlErrorMsg.isDisplayed(), true);
	}

	@BeforeClass
	public void beforeClass() throws IOException {

		File configFile = new File("src/main/resources/config.properties");
		FileInputStream fileInput = new FileInputStream(configFile);
		properties.load(fileInput);
		fileInput.close();

		File file = new File("C:\\Automation\\chromedriver_win32\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://preview.revature.com/core/");

		LoginPage loginPage = new LoginPage(driver);
		loginPage.username.sendKeys(properties.getProperty("username"));
		loginPage.password.sendKeys(properties.getProperty("password"));
		loginPage.submit.click();

		while (driver.getTitle().equals("Revature | Admin")) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		System.out.println(driver.getTitle());
		Assert.assertNotEquals(driver.getTitle(), "Revature | Admin");

		HomePage hpage = new HomePage(driver);
		
		hpage.QuestionsDropdown.click();
		
	      WebDriverWait wait = new WebDriverWait(driver, 8);
    	 
    	 wait.until(ExpectedConditions.visibilityOf(hpage.ImportQuestionLink));
		 hpage.ImportQuestionLink.click();

	}

	@AfterClass
	public void afterClass() {
		HomePage hPage = new HomePage(driver);
		hPage.settingsDropdown.click();
		hPage.logOutButton.click();
		driver.quit();

	}

	@BeforeMethod
	public void beforeMethod() {

		HomePage hpage = new HomePage(driver);
		
		while(!hpage.QuestionsDropdown.isDisplayed()) {
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			if(hpage.QuestionsDropdown.isDisplayed()) {
				hpage.QuestionsDropdown.click();
				break;
			}
		}
		
		while(!hpage.ImportQuestionLink.isDisplayed()) {
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			if(hpage.ImportQuestionLink.isDisplayed()) {
				hpage.ImportQuestionLink.click();
				break;
			}
		}
	}

}
