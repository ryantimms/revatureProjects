/**
 * 
 */
var app = angular.module("ersApp", []);
//Employee View All
app.controller('empViewAllController', function($scope, $http) {
	$scope.getDataFromServer = function() {

		$http.get('/EmployeeReimbursementSystem/viewAll.ajax').then(ifSuccess,
				ifError);

		function ifSuccess(response) {
			$scope.list = response.data;
		}

		function ifError(response) {
			console.log('cannot get data');
		}
	}
});
//Employee Pending
app.controller('empViewPendingController', function($scope, $http) {
	$scope.getDataFromServer = function() {

		$http.get('/EmployeeReimbursementSystem/viewAll.pending').then(
				ifSuccess, ifError);

		function ifSuccess(response) {
			$scope.list = response.data;
		}

		function ifError(response) {
			console.log('cannot get data');
		}
	}
});
//View Employees
app.controller('managerViewEmployeesController', function($scope, $http) {
	$scope.getDataFromServer = function() {

		$http.get('/EmployeeReimbursementSystem/viewEmp.managerviewemp').then(
				ifSuccess, ifError);

		function ifSuccess(response) {
			$scope.list = response.data;
		}

		function ifError(response) {
			console.log('cannot get data');
		}
	}
});
//View Pending Manager
app.controller('managerViewPendingController', function($scope, $http) {
	$scope.getDataFromServer = function() {

		$http.get('/EmployeeReimbursementSystem/viewReimb.managerpending')
				.then(ifSuccess, ifError);

		function ifSuccess(response) {
			$scope.list = response.data;
		}

		function ifError(response) {
			console.log('cannot get data');
		}
	}
});
//View Resolved Manager
app.controller('managerViewResolvedController', function($scope, $http) {
	$scope.getDataFromServer = function() {

		$http.get('/EmployeeReimbursementSystem/viewReimb.managerresolved').then(ifSuccess, ifError);

		function ifSuccess(response) {
			$scope.list = response.data;
		}

		function ifError(response) {
			console.log('cannot get data');
		}
	}
});


//Search Controller
app.controller('managerSearchController', function($scope, $http) {
	$scope.getDataFromServer = function() {

		$http.get('/EmployeeReimbursementSystem/viewReimb.search').then(
				ifSuccess, ifError);

		function ifSuccess(response) {
			$scope.list = response.data;
		}

		function ifError(response) {
			console.log('cannot get data');
		}
	}
});