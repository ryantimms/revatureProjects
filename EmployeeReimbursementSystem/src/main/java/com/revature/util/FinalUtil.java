package com.revature.util;

public class FinalUtil {
	//Basic string constants
	public static final String HTTP_GET = "GET";
	public static final String EMPTY_STRING = "";
	
	// Ajax message values
	public static final String USERNAME_TAKEN = "USERNAME TAKEN";
	public static final String USERNAME_AVAILABLE = "USERNAME AVAILABLE";
	public static final String NOT_IMPLEMENTED = "NOT_IMPLEMENT";
}
