#Author: Ryan Timms
#Keywords Summary : Test all of the question types
#Feature: True/False, Multiple Choice, Best Choice, Numerical

@tag
Feature: Create New Question

Background:
Given I am logged in
And I am on the create new question page

@tag1
Scenario: I want to make a True/False question
When I choose true false as my question type
	And I fill out the true false form correctly
	And I click the save button
Then The true false question I saved appears in the all questions list

@tag2
Scenario: I want to make a Multiple Choice question
When I choose multiple choice as my question type
And I fill out the multiple choice form correctly
And I save my multiple choice question
Then The multiple choice question I saved appears in the question list

@tag3
Scenario: I want to make a Best Choice question
When I choose best choice as my question type
And I fill out the best choice form correctly
And I save my best choice question
Then The best choice question I saved appears in the question list

@tag4
Scenario: I want to make a Numerical question
When I choose numerical as my question type
And I fill out the numerical form correctly
And I save my numerical question
Then The numerical question I saved appears in the question list
