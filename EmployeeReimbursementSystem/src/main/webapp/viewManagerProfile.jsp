<%@page import="com.revature.model.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<link href="background.css" rel="stylesheet">
<title>View Profile</title>
</head>
<body>
	<nav class="navbar">
	<div class="navbar navbar-inverse fixed-top">
		<div class="navbar-header">
			<a class="navbar-brand">Revature</a>
		</div>
		<ul class="nav navbar-nav">
			<li><a href="home.html">Home</a></li>

			<li><a href="ManagerReimbHome.jsp">Reimbursements</a></li>

			<li><a>View Profile</a></li>
			
			<li><a href="viewEmployees.jsp">View Employees</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="/EmployeeReimbursementSystem/LogOut.do">Log Out</a></li>
		</ul>
	</div>
	</nav>

	<div class="container">
		<div class="row">
			<%
				User user = (User) request.getSession().getAttribute("loggedUser");
			%>
			User name:
			<%=user.getUserName()%>
			<br> First name:
			<%=user.getFirstName()%>
			<br> Last name:
			<%=user.getLastName()%>
			<br> email:
			<%=user.getEmail()%>

		</div>
		<div class="row">
			Update User Information:
			<form method="POST" action="updateInfo.do">
				User name: <input placeholder="username" name="username"
					value="<%=user.getUserName()%>" /> First Name: <input
					placeholder="First name" name="firstName"
					value="<%=user.getFirstName()%>" /> Last Name: <input
					placeholder="Last name" name="lastName"
					value="<%=user.getLastName()%>" /> Email: <input
					placeholder="email" name="email" value="<%=user.getEmail()%>" />
				<input type="submit" value="Submit edits" />
			</form>
		</div>

	</div>

</body>
</html>