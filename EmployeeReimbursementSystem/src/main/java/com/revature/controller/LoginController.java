package com.revature.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.revature.dao.ReimbursementDaoImp;
import com.revature.model.Reimbursement;
import com.revature.model.User;
import com.revature.services.UserService;
import com.revature.util.FinalUtil;

public class LoginController {
	public static String login(HttpServletRequest request) {

		// Return to login view for unsecured get method
		if (request.getMethod().equals(FinalUtil.HTTP_GET)) {
			return "Login.jsp";
		}

		// POST method is secured, check if user credentials are valid

		// Get a user object using information received from the request compared
		// against the database
		User loggedUser = UserService.getInstance()
				.login(new User(request.getParameter("inputUsername"), request.getParameter("inputPassword")));

		// If no match is found the new loggedUser object will have an empty user name
		// therefore the credentials are invalid
		if (loggedUser.getUserName().equals("") || loggedUser.getUserName() == null) {
			return "Login.jsp";
		} else {
			if (loggedUser.getRole_id() == 1) {
				// Binds the loggedUser instance to this session
				request.getSession().setAttribute("loggedUser", loggedUser);
				List<Reimbursement> allPendingReimbs = ReimbursementDaoImp.getInstance().selectAllWaiting();
				request.getSession().setAttribute("allPendingReimbs", allPendingReimbs);

				return "home.html";
			} else {
				request.getSession().setAttribute("loggedUser", loggedUser);
				List<Reimbursement> allPendingReimbs = ReimbursementDaoImp.getInstance().selectAllWaiting();
				request.getSession().setAttribute("allPendingReimbs", allPendingReimbs);

				return "ManagerHome.html";
			}
		}

	}
}
