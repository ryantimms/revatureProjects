package com.revature.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {
	static {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			e.getStackTrace();
//			LogUtil.logger.warn("Exception thrown adding oracle driver.",e);
		}
	}
	public static Connection getConnection() throws SQLException {
		String url="jdbc:oracle:thin:@reimbursementdb.cmlwrs4w5a1j.us-east-2.rds.amazonaws.com:1521:ORCL";
		String userName="RyanTimms";
		String password="p4ssw0rd";
		return DriverManager.getConnection(url, userName, password);
	}
}
