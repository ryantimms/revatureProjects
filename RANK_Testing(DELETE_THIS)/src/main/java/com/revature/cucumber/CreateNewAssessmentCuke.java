package com.revature.cucumber;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.revature.factory.AssessmentNamePage;
import com.revature.factory.AssessmentsCreateNew;
import com.revature.factory.AssessmentsPage;
import com.revature.factory.HomePage;
import com.revature.factory.LoginPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateNewAssessmentCuke {
	Properties properties = new Properties();
	WebDriver driver;
	AssessmentsCreateNew aCreateNew;
	
	@Given("^I am on the Create New Assessment Page$")
	public void i_am_on_the_Create_New_Assessment_Page() throws Throwable {
		File configFile = new File("src/main/resources/config.properties");
		FileInputStream fileInput = new FileInputStream(configFile);
		properties.load(fileInput);
		fileInput.close();

		//TODO: this needs to be a relative path.
		File file = new File("C:\\Automation\\chromedriver_win32\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://preview.revature.com/core/");

		LoginPage loginPage = new LoginPage(driver);
		loginPage.username.sendKeys(properties.getProperty("username"));
		loginPage.password.sendKeys(properties.getProperty("password"));
		loginPage.submit.click();
		while (driver.getTitle().equals("Revature | Admin")) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

		HomePage hPage = new HomePage(driver);
		hPage.AssessmentsLink.click();
		AssessmentsPage aPage = new AssessmentsPage(driver);
		aPage.createNewButton.click();
	}

	@Given("^I have filled in the mandatory fields$")
	public void i_have_filled_in_the_mandatory_fields() throws Throwable {
		aCreateNew = new AssessmentsCreateNew(driver);
		//fill in mandatory fields with what should be acceptable input
		//activity points and pass % have default values
		aCreateNew.assessmentNameInput.clear();
		//so name doesn't clash on multiple runs
		aCreateNew.assessmentNameInput.sendKeys((properties.getProperty("alphaNumUnderscoreTitle")));
		aCreateNew.tagsInput.clear();
		aCreateNew.tagsInput.sendKeys(properties.getProperty("JTAtag"));
		aCreateNew.tagsInput.sendKeys(Keys.RETURN);
		aCreateNew.tagsInput.sendKeys(properties.getProperty("ACNtag"));
		aCreateNew.tagsInput.sendKeys(Keys.RETURN);
		aCreateNew.tagsInput.sendKeys(properties.getProperty("RANKtag"));
		aCreateNew.tagsInput.sendKeys(Keys.RETURN);
		aCreateNew.slugInput.clear();
		aCreateNew.slugInput.sendKeys(properties.getProperty("alphaNumUnderscoreTitle"));
		Select levelDropdown = new Select(aCreateNew.levelDropdown);
		levelDropdown.selectByValue(properties.getProperty("level100value"));
		aCreateNew.categoryDropdown.click();
		aCreateNew.dotNetCategory.click();
		//aCreateNew.container.click();
	}

	@When("^I click Save and Continue$")
	public void i_click_Save_and_Continue() throws Throwable {
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.saveContinueButton.click();
	}

	@When("^change the text in a field$")
	public void change_the_text_in_a_field() throws Throwable {
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.assessmentNameInput.clear();
		aCreateNew.assessmentNameInput.sendKeys(properties.getProperty("standardName"));
	}

	@When("^click Save and Continue again$")
	public void click_Save_and_Continue_again() throws Throwable {
		aCreateNew = new AssessmentsCreateNew(driver);
		Assert.assertTrue(aCreateNew.saveContinueButton.isEnabled());
		Assert.assertTrue(aCreateNew.saveContinueButton.isDisplayed());
		while(aCreateNew.saveContinueButton.isDisplayed() == false) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		aCreateNew.assessmentHeading.click();
		aCreateNew.saveContinueButton.click();
	}

	@Then("^my changes should be visible in the form$")
	public void my_changes_should_be_visible_in_the_form() throws Throwable {
		aCreateNew = new AssessmentsCreateNew(driver);
	    Assert.assertEquals(aCreateNew.assessmentNameInput.getAttribute("value"), properties.getProperty("standardName"));
	    HomePage hPage = new HomePage(driver);
		hPage.settingsDropdown.click();
		hPage.logOutButton.click();
		driver.quit();
	}

	@When("^I click save as draft$")
	public void i_click_save_as_draft() throws Throwable {
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew = new AssessmentsCreateNew(driver);
		aCreateNew.saveAsDraftButton.click();
	}

	@Then("^I should see my assessment in the Assessments table$")
	public void i_should_see_my_assessment_in_the_Assessments_table() throws Throwable {
		HomePage hPage = new HomePage(driver);
	    hPage.AssessmentsLink.click();
	    AssessmentsPage aPage = new AssessmentsPage(driver);
	    aPage.assessmentNameLink.click();
	    AssessmentNamePage aNamePage = new AssessmentNamePage(driver);
	    Assert.assertTrue(aNamePage.heading.getText().contains(properties.getProperty("alphaNumUnderscoreTitle")));
	    
	    HomePage homePage = new HomePage(driver);
		homePage.settingsDropdown.click();
		homePage.logOutButton.click();
		driver.quit();
	}

}
