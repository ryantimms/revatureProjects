package com.revature.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AssessmentsCreateNew {

	public AssessmentsCreateNew(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[@id=\\\"asmtMgmtForm:savePubLink\\\"]")
	public WebElement publishButton;

	@FindBy(id = "asmtMgmtForm:addSaveCon")
	public WebElement saveContinueButton;

	@FindBy(id = "asmtMgmtForm:saveDraftLink")
	public WebElement saveAsDraftButton;

	// May need to be changed
	@FindBy(xpath = "//*[@id=\"asmtMgmtForm\"]/div/div[1]/div/a[4]")
	public WebElement backButton;
	
	@FindBy(xpath = "//*[@id=\"asmtMgmtForm\"]/div/div[1]/h1")
	public WebElement assessmentHeading;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:titleId\"]")
	public WebElement assessmentNameInput;

	// May need to be changed
	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtMainCntntPanId\"]/div[1]/div[2]/div/div[1]/span/div/input[2]")
	public WebElement tagsInput;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtMainCntntPanId\"]/div[1]/div[2]/div/div[1]/span/div")
	public WebElement tagsDivInput;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:lvl-ovr-id:0\"]")
	public WebElement levelOverrideOnRadio;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:lvl-ovr-id:1\"]")
	public WebElement levelOverrideOffRadio;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:slugId\"]")
	public WebElement slugInput;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:descriptionId\"]")
	public WebElement descriptionTextArea;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:actPtsId\"]")
	public WebElement activityPointsInput;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:noOfAtmpts\"]")
	public WebElement maxNoOfAttemptsInput;
	
	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtMainCntntPanId\"]/div[1]/div[4]/div/div[1]/div/div/div/button[1]")
	public WebElement maxNoAttemptsUpButton;
	
	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtMainCntntPanId\"]/div[1]/div[4]/div/div[1]/div/div/div/button[2]")
	public WebElement maxNoAttemptsDownButton;
	
	// May need to be changed
	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtMainCntntPanId\"]/div[1]/div[8]/div/div[1]/span/div/input[2]")
	public WebElement metaKeywordsInput;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:metaDesc\"]")
	public WebElement metaDescriptionTextArea;

	@FindBy(xpath = "//*[@id=\"dropzoneFileUploadDiv\"]")
	public WebElement fileDropZone;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:atchQsnLinkId\"]")
	public WebElement addQuestionsButton;
	
	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:lvlId\"]")
	public WebElement levelDropdown;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:lvlId\"]")
	public WebElement levelDropdowns;

	// Category dropdown gets clicked and then offer a search input and clickable
	// choices  //*[@id=\"s2id_asmtMgmtForm:catId\"]/a
	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtMainCntntPanId\"]/div[2]")
	public WebElement container;

	@FindBy(xpath = "//*[@id=\"s2id_asmtMgmtForm:catId\"]")
	public WebElement categoryDropdown;

	@FindBy(xpath = "//*[@id=\"s2id_autogen1_search\"]")
	public WebElement categoryDropdownSearchInput;
	
	@FindBy(id = "//*[@id=\"select2-results-1\"]/li[1]")
	public WebElement selectCategory;

	// Not sure if each drowndrop choice needs a webelement or not
	@FindBy(xpath = "//*[@id=\"select2-results-1\"]/li[2]")
	public WebElement dotNetCategory;

	@FindBy(xpath = "//*[@id=\"select2-results-1\"]/li[3]")
	public WebElement bigDataCategory;

	@FindBy(xpath = "//*[@id=\"select2-results-1\"]/li[4]")
	public WebElement devOpsCategory;

	@FindBy(xpath = "//*[@id=\"select2-results-1\"]/li[5]")
	public WebElement dynamicsCRMCategory;

	@FindBy(xpath = "//*[@id=\"select2-results-1\"]/li[6]")
	public WebElement generalCategory;

	@FindBy(xpath = "//*[@id=\"select2-results-1\"]/li[7]")
	public WebElement javaCategory;

	@FindBy(xpath = "//*[@id=\"select2-results-1\"]/li[8]")
	public WebElement javaCertificationCategory;

	@FindBy(xpath = "//*[@id=\"select2-results-1\"]/li[9]")
	public WebElement oracleFusionMiddlewareCategory;

	@FindBy(xpath = "//*[@id=\"select2-results-1\"]/li[10]")
	public WebElement pegaCategory;

	@FindBy(xpath = "//*[@id=\"select2-results-1\"]/li[11]")
	public WebElement salesForceCategory;

	@FindBy(xpath = "//*[@id=\"select2-results-1\"]/li[12]")
	public WebElement sqlCategory;

	@FindBy(xpath = "//*[@id=\"select2-results-1\"]/li[13]")
	public WebElement testingCategory;

	@FindBy(xpath = "//*[@id=\"select2-results-1\"]/li[14]")
	public WebElement webToolsCategory;
	// ---------end of categories list---------

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:passPerId\"]")
	public WebElement passPercentInput;
	
	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtMainCntntPanId\"]/div[2]/div[3]/div/div[1]/div/div/div/button[1]")
	public WebElement passPercentUpButton;
	
	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtMainCntntPanId\"]/div[2]/div[3]/div/div[1]/div/div/div/button[2]")
	public WebElement passPercentDownButton;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:timEnbId:0\"]")
	public WebElement timerEnabledYesRadio;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:timEnbId:1\"]")
	public WebElement timerEnabledNoRadio;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:sufQsnId:0\"]")
	public WebElement shuffleQuestionsYesRadio;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:sufQsnId:1\"]")
	public WebElement shuffleQuestionsNoRadio;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:sufAnsId:0\"]")
	public WebElement shuffleAnswersYesRadio;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:sufAnsId:1\"]")
	public WebElement shuffleAnswersNoRadio;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:disScorId:0\"]")
	public WebElement displayScoreYesRadio;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:disScorId:1\"]")
	public WebElement displayScoreNoRadio;

	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:asmtReviewId\"]")
	public WebElement allowAttemptReviewCheckbox;

	// Only usable if the allowAttemptReviewCheckbox is checked
	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:showWhetherCrct\"]")
	public WebElement showWhetherCorrectCheckbox;

	// Only usable if the showWhetherCorrectCheckbox is checked
	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:dispCorAnsPassId\"]")
	public WebElement displayCorrectPassedCheckbox;

	// Only usable if the showWhetherCorrectCheckbox is checked
	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:dispCorAnsFailId\"]")
	public WebElement displayCorrectFailedCheckbox;

	// Only usable if the displayCorrectFailedCheckbox OR the
	// displayCorrectPassedCheckbox are checked
	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:showExpId:0\"]")
	public WebElement showExplanationYesRadio;

	// Only usable if the displayCorrectFailedCheckbox OR the
	// displayCorrectPassedCheckbox are checked
	@FindBy(xpath = "//*[@id=\"asmtMgmtForm:showExpId:1\"]")
	public WebElement showExplanationNoRadio;

	/*
	 * TODO: not sure if the instructions word editor / functionality is within the
	 * scope of our testing
	 */

	/*
	 * TODO: Instructions uses an html tag to create a word editor Not sure about
	 * this web element functionality May need to be changed
	 */
	@FindBy(xpath = "/html/body/p")
	public WebElement instructionsInput;

	@FindBy(xpath = "//*[@id=\"cke_12\"]/span[1]")
	public WebElement instructionsBoldTextButton;

	@FindBy(xpath = "//*[@id=\"cke_13\"]/span[1]")
	public WebElement instructionsItalicTextButton;

	@FindBy(xpath = "//*[@id=\"cke_14\"]/span[1]")
	public WebElement instructionsStrikeTextButton;

	@FindBy(xpath = "//*[@id=\"cke_15\"]/span[1]")
	public WebElement instructionsRemoveFormatTextButton;

	@FindBy(xpath = "//*[@id=\"cke_16_label\"]")
	public WebElement instructionsSourceButton;

	@FindBy(xpath = "//*[@id=\"cke_17\"]/span[1]")
	public WebElement instructionsNumberedListButton;

	@FindBy(xpath = "//*[@id=\"cke_18\"]/span[1]")
	public WebElement instructionsBulletedListButton;

	@FindBy(xpath = "//*[@id=\"cke_19\"]/span[1]")
	public WebElement instructionsDecreaseIndentButton;

	@FindBy(xpath = "//*[@id=\"cke_20\"]/span[1]")
	public WebElement instructionsIncreaseIndentButton;

	@FindBy(xpath = "//*[@id=\"cke_21\"]/span[1]")
	public WebElement instructionsBlockQuoteButton;

	@FindBy(xpath = "//*[@id=\"cke_9_text\"]")
	public WebElement instructionsStyleDropdown;

	@FindBy(xpath = "//*[@id=\"cke_10_text\"]")
	public WebElement instructionsParagrapFormatButton;

	@FindBy(xpath = "//*[@id=\"cke_22\"]/span[1]")
	public WebElement instructionsInsertYoutubeButton;

	@FindBy(xpath = "//*[@id=\"cke_23\"]/span[1]")
	public WebElement instructionsImageButton;

	@FindBy(xpath = "//*[@id=\"cke_24\"]/span[1]")
	public WebElement instructionsCodeSnippetButton;
	// ------end of instructions-----------------


}
