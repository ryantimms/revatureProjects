package com.revature.cucumber;

import java.io.File;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.revature.factory.AllQuestions;
import com.revature.factory.AllQuestions_CreateNew;
import com.revature.factory.HomePage;
import com.revature.factory.LoginPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateNewQuestion {
	WebDriver driver;
	Properties properties = new Properties();

	// --------=-=-=-=-----Background------=-=-=-=---------
	@Given("^I am logged in$")
	public void i_am_logged_in() throws Throwable {
		File configFile = new File("src/main/resources/config.properties");
		FileInputStream fileInput = new FileInputStream(configFile);
		properties.load(fileInput);
		fileInput.close();

		File file = new File("C:\\Automation\\chromedriver_win32\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://preview.revature.com/core/");

		LoginPage login = new LoginPage(driver);
		login.username.sendKeys(properties.getProperty("username"));
		login.password.sendKeys(properties.getProperty("password"));
		login.submit.click();
		while (driver.getTitle().equals(properties.getProperty("loginPageTitle"))) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}

	@Given("^I am on the create new question page$")
	public void i_am_on_the_create_new_question_page() throws Throwable {
		HomePage home = new HomePage(driver);
		home.QuestionsDropdown.click();
		home.AllQuestionsLink.click();

		AllQuestions aQPage = new AllQuestions(driver);
		aQPage.CreateNew.click();
		while (driver.getCurrentUrl().equals(properties.getProperty("allQuestionsURL"))) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}
	// -------=-=-=-=-----END:Background-----------=-=-=-=-----------

	// -------=-=-=-=----True/False----------------=-=-=-=----------
	@When("^I choose true false as my question type$")
	public void i_choose_true_false_as_my_question_type() throws Throwable {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		Select types = new Select(createNewPage.questionType);
		types.selectByIndex(2);
	}

	@When("^I fill out the true false form correctly$")
	public void i_fill_out_the_true_false_form_correctly() throws Throwable {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		createNewPage.questionTitle.sendKeys(properties.getProperty("newQuestionTitle"));
		createNewPage.category.sendKeys(Keys.ARROW_DOWN);
		createNewPage.level.sendKeys(Keys.ARROW_DOWN);
		createNewPage.skillPoints.sendKeys(properties.getProperty("skillPoints"));
		createNewPage.score.sendKeys(properties.getProperty("score"));
		createNewPage.duration.click();
		createNewPage.incrementMinute.click();
		createNewPage.tags.sendKeys(properties.getProperty("newQuestionTFTag"));
		createNewPage.trueExplanation.sendKeys(properties.getProperty("newQuestionTrueExp"));
		createNewPage.falseExplanation.sendKeys(properties.getProperty("newQuestionFalseExp"));
		createNewPage.trueRadio.click();
	}

	@When("^I click the save button$")
	public void i_click_the_save_button() throws Throwable {
		// TODO: NEED TO FIX
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", createNewPage.saveButton);
		while(driver.getCurrentUrl().equals(properties.getProperty("newQuestionURL"))) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}

	@Then("^The true false question I saved appears in the all questions list$")
	public void the_true_false_question_I_saved_appears_in_the_all_questions_list() throws Throwable {
		AllQuestions aQPage = new AllQuestions(driver);
		aQPage.Search.sendKeys(properties.getProperty("newQuestionTFTag"));
		aQPage.Search.sendKeys(Keys.RETURN);
		Assert.assertTrue(aQPage.rows.size() > 0);
		aQPage.deleteQuestionButton.click();
		driver.quit();
	}

	// -------=-=-=-=----END:True/False----------=-=-=-=--------------

	// ---------=-=-=-=---Multiple Choice--------=-=-=-=------------
	@When("^I choose multiple choice as my question type$")
	public void i_choose_multiple_choice_as_my_question_type() throws Throwable {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		Select types = new Select(createNewPage.questionType);
		types.selectByIndex(3);
	}

	@When("^I fill out the multiple choice form correctly$")
	public void i_fill_out_the_multiple_choice_form_correctly() throws Throwable {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		createNewPage.questionTitle.sendKeys(properties.getProperty("newQuestionTitle"));
		createNewPage.category.sendKeys(Keys.ARROW_DOWN);
		createNewPage.level.sendKeys(Keys.ARROW_DOWN);
		createNewPage.skillPoints.sendKeys(properties.getProperty("skillPoints"));
		createNewPage.score.sendKeys(properties.getProperty("score"));
		createNewPage.duration.click();
		createNewPage.incrementMinute.click();
		createNewPage.tags.sendKeys("newQuestionMCTag");
		createNewPage.mChoiceExp.sendKeys(properties.getProperty("newQuestionMCContent"));
	}

	@When("^I save my multiple choice question$")
	public void i_save_my_multiple_choice_question() throws Throwable {
		// TODO: NEED TO FIX
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", createNewPage.saveButton);
		while(driver.getCurrentUrl().equals(properties.getProperty("newQuestionURL"))) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

	}

	@Then("^The multiple choice question I saved appears in the question list$")
	public void the_multiple_choice_question_I_saved_appears_in_the_question_list() throws Throwable {
		AllQuestions aQPage = new AllQuestions(driver);
		aQPage.Search.sendKeys(properties.getProperty("newQuestionMCTag"));
		aQPage.Search.sendKeys(Keys.RETURN);
		Assert.assertTrue(aQPage.rows.size() > 0);
		aQPage.deleteQuestionButton.click();
		driver.quit();
	}
	// ---------=-=-=-=------END: Multiple Choice-----------=-=-=-=-----------

	// ------------=-=-=-=----Best Choice--------------=-=-=-=-------------
	@When("^I choose best choice as my question type$")
	public void i_choose_best_choice_as_my_question_type() throws Throwable {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		Select types = new Select(createNewPage.questionType);
		types.selectByIndex(1);
	}

	@When("^I fill out the best choice form correctly$")
	public void i_fill_out_the_best_choice_form_correctly() throws Throwable {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		createNewPage.questionTitle.sendKeys(properties.getProperty("newQuestionTitle"));
		createNewPage.category.sendKeys(Keys.ARROW_DOWN);
		createNewPage.level.sendKeys(Keys.ARROW_DOWN);
		createNewPage.skillPoints.sendKeys(properties.getProperty("skillPoints"));
		createNewPage.score.sendKeys(properties.getProperty("score"));
		createNewPage.duration.click();
		createNewPage.incrementMinute.click();
		createNewPage.tags.sendKeys("newQuestionBCTag");
		createNewPage.mChoiceExp.sendKeys(properties.getProperty("newQuestionMCContent"));
	}

	@When("^I save my best choice question$")
	public void i_save_my_best_choice_question() throws Throwable {
		// TODO: NEED TO FIX
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", createNewPage.saveButton);
		while(driver.getCurrentUrl().equals(properties.getProperty("newQuestionURL"))) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

	}

	@Then("^The best choice question I saved appears in the question list$")
	public void the_best_choice_question_I_saved_appears_in_the_question_list() throws Throwable {
		AllQuestions aQPage = new AllQuestions(driver);
		aQPage.Search.sendKeys(properties.getProperty("newQuestionBCTag"));
		aQPage.Search.sendKeys(Keys.RETURN);
		Assert.assertTrue(aQPage.rows.size() > 0);
		aQPage.deleteQuestionButton.click();
		driver.quit();
	}
	// ----------=-=-=-=--END: Best Choice----------------=-=-=-=-------

	// -----------=-=-=-=-Numerical----------------=-=-=-=--------------
	@When("^I choose numerical as my question type$")
	public void i_choose_numerical_as_my_question_type() throws Throwable {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		Select types = new Select(createNewPage.questionType);
		types.selectByIndex(4);
	}

	@When("^I fill out the numerical form correctly$")
	public void i_fill_out_the_numerical_form_correctly() throws Throwable {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		createNewPage.questionTitle.sendKeys(properties.getProperty("newQuestionTitle"));
		createNewPage.category.sendKeys(Keys.ARROW_DOWN);
		createNewPage.level.sendKeys(Keys.ARROW_DOWN);
		createNewPage.skillPoints.sendKeys(properties.getProperty("skillPoints"));
		createNewPage.score.sendKeys(properties.getProperty("score"));
		createNewPage.duration.click();
		createNewPage.incrementMinute.click();
		createNewPage.tags.sendKeys("newQuestionNUMTag");
		createNewPage.numericalAnswer.sendKeys(properties.getProperty("newQuestionNumericalAnswer"));
	}

	@When("^I save my numerical question$")
	public void i_save_my_numerical_question() throws Throwable {
		// TODO: NEED TO FIX
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", createNewPage.saveButton);
		while(driver.getCurrentUrl().equals(properties.getProperty("newQuestionURL"))) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

	}

	@Then("^The numerical question I saved appears in the question list$")
	public void the_numerical_question_I_saved_appears_in_the_question_list() throws Throwable {
		AllQuestions aQPage = new AllQuestions(driver);
		aQPage.Search.sendKeys(properties.getProperty("newQuestionNUMTag"));
		aQPage.Search.sendKeys(Keys.RETURN);
		Assert.assertTrue(aQPage.rows.size() > 0);
		aQPage.deleteQuestionButton.click();
		driver.quit();
	}
	// ----------=-=-=-=---END: Numerical--------------=-=-=-=--------

}
