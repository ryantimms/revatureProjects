#Author: Ryan Timms
#Keywords Summary : Test searching and viewing an assessment

@tag
Feature: View a specific assessment
	I want to use this template for my feature file

@tag1
Scenario: A user wants to view a specific assessment by searching
Given I am logged in to an Admin account
	And I am able to view the main assessment page
When I type a search query into the search bar
	And I click on one of the results
Then I am able to view the assessment I clicked on
