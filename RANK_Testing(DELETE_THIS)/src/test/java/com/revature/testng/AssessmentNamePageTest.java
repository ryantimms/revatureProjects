package com.revature.testng;

import org.openqa.selenium.WebDriver
;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.revature.factory.AssessmentNamePage;
import com.revature.factory.HomePage;
import com.revature.factory.LoginPage;

import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;


public class AssessmentNamePageTest {
	Properties properties = new Properties();
	public static WebDriver driver;
	
    @Test(priority=3)
    public void testEditButton() {
    	 SoftAssert softAssert = new SoftAssert();
    	 AssessmentNamePage anp = new  AssessmentNamePage(driver);
    	 anp.editButton.click();
    	 
         WebDriverWait wait = new WebDriverWait(driver, 5);
    	 
    	 Boolean URLContainsClone =  wait.until(ExpectedConditions.urlContains("Edit"));
    	 softAssert.assertEquals(URLContainsClone.booleanValue(), true);
    }
    
    @Test(priority=2)
    public void testCloneButton() {
    	 SoftAssert softAssert = new SoftAssert();
    	 AssessmentNamePage anp = new  AssessmentNamePage(driver);
    	 anp.cloneButton.click();
    	 //kaycis comment
    	 WebDriverWait wait = new WebDriverWait(driver, 5);
    	 
    	 Boolean URLContainsClone =  wait.until(ExpectedConditions.urlContains("clone"));
    	 softAssert.assertEquals(URLContainsClone.booleanValue(), true);
    }
    
//    @Test(priority=4)
//    public void testDeleteButton() {
//    	 SoftAssert softAssert = new SoftAssert();
//    	 AssessmentNamePage anp = new  AssessmentNamePage(driver);
//    	 
//    	 String currURL = driver.getCurrentUrl();
//    	 
//    	 anp.deleteButton.click();
//    	 
//    	 WebDriverWait wait = new WebDriverWait(driver, 5);
//    	 WebElement noDel = wait.until(ExpectedConditions.elementToBeClickable(anp.noDelete));
//    	   	 
//    	 noDel.click();
//    	 WebElement del = wait.until(ExpectedConditions.elementToBeClickable(anp.deleteButton));
//    	 softAssert.assertEquals(currURL, driver.getCurrentUrl());
//    	 del.click();
//    	 
//    	 WebElement closeMod = wait.until(ExpectedConditions.elementToBeClickable(anp.closeModal));
//    	 closeMod.click();
//    	 WebElement del2 = wait.until(ExpectedConditions.elementToBeClickable(anp.deleteButton));
//    	 softAssert.assertEquals(currURL, driver.getCurrentUrl());
//    	 del2.click();
//    	 
//    	 WebElement yesDel =  wait.until(ExpectedConditions.elementToBeClickable(anp.yesDelete));
//    	 yesDel.click();
//    	 //see what happens when delete button is clicked
//    	 wait.until(ExpectedConditions.visibilityOf(anp.deleteAccessDeniedMsg));
//    	 softAssert.assertEquals(anp.deleteAccessDeniedMsg.toString().contains("denied"), true);
//    	 
//    }
    
    @Test(priority=1)
    public void testBackButton() {
    	 SoftAssert softAssert = new SoftAssert();
    	 AssessmentNamePage anp = new  AssessmentNamePage(driver);
    	 anp.backButton.click();
    	 WebDriverWait wait = new WebDriverWait(driver, 5);
    	 wait.until(ExpectedConditions.urlToBe("https://preview.revature.com/core/admin/pages/assessments"));
    	 softAssert.assertEquals("https://preview.revature.com/core/admin/pages/assessments", driver.getCurrentUrl());
    }
    
    @Test(priority=0)
    public void testSlug() {
    	 SoftAssert softAssert = new SoftAssert();
    	 AssessmentNamePage anp = new  AssessmentNamePage(driver);
    	 String href = anp.slug.getAttribute("href");
    	 anp.slug.click();
    	     	 
    	 ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
    	 driver.switchTo().window(tabs2.get(1));
 	     System.out.println(driver.getTitle());
    	 WebDriverWait wait = new WebDriverWait(driver, 10);
    	 wait.until(ExpectedConditions.urlToBe(href));
    	 softAssert.assertEquals(href, driver.getCurrentUrl());
    	 
    	 
    	 driver.close();
  	     driver.switchTo().window(tabs2.get(0));  
    	 //System.out.println((String)beforeTabOpens.toArray()[0]);
    }
    
    
    @BeforeClass
	public void beforeClass() throws IOException {

		File configFile = new File("src/main/resources/config.properties");
		FileInputStream fileInput = new FileInputStream(configFile);
		properties.load(fileInput);
		fileInput.close();

		File file = new File("C:\\Automation\\chromedriver_win32\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://preview.revature.com/core/");
		
		LoginPage loginPage = new LoginPage(driver);
		loginPage.username.sendKeys(properties.getProperty("username"));
		loginPage.password.sendKeys(properties.getProperty("password"));
		loginPage.submit.click();
		
		while(driver.getTitle().equals("Revature | Admin")) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		System.out.println(driver.getTitle());
		Assert.assertNotEquals(driver.getTitle(), "Revature | Admin");
		
		HomePage hpage = new HomePage(driver);
		hpage.AssessmentsLink.click();
		AssessmentNamePage anp = new AssessmentNamePage(driver);
		anp.rankAssessment.click();

	}

	@AfterClass
	public void afterClass() {
		HomePage hPage = new HomePage(driver);
		hPage.settingsDropdown.click();
		hPage.logOutButton.click();
		driver.quit();

	}
	
	
	@BeforeMethod
	public void beforeMethod() {
		
		HomePage hpage = new HomePage(driver);
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		hpage.AssessmentsLink.click();
		AssessmentNamePage anp = new AssessmentNamePage(driver);
		anp.rankAssessment.click();
		
	}
	
}

