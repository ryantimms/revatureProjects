package com.revature.controller;

import javax.servlet.http.HttpServletRequest;

import com.revature.model.Reimbursement;
import com.revature.model.User;
import com.revature.services.ReimbursementService;

public class ResolveController {
public static String resolveReimb(HttpServletRequest request) {
	User loggedUser= (User) request.getSession().getAttribute("loggedUser");
	int reimbID = Integer.parseInt(request.getParameter("reimbId"));
	int statusID = Integer.parseInt(request.getParameter("statusId"));
	if(ReimbursementService.getInstance().resolve(new Reimbursement(reimbID, loggedUser.getUser_id(), statusID))) {
		return "ManagerReimbHome.jsp";
	}
	return "ManagerReimbHome.jsp";
}
}
