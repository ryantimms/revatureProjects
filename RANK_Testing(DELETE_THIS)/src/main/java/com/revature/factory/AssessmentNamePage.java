package com.revature.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AssessmentNamePage {
	
	public AssessmentNamePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//*[@id=\"asmtMgmtForm\"]/div/div[1]/div/a[1]")
	public WebElement editButton;
	
	@FindBy(xpath="//*[@id=\"asmtMgmtForm\"]/div/div[1]/div/a[2]")
	public WebElement cloneButton;
	
	@FindBy(xpath="//*[@id=\"asmtMgmtForm:delAsmtBtn\"]")//*[@id="asmtMgmtForm:delAsmtBtn"]
	public WebElement deleteButton;
	
	@FindBy (xpath="//*[@id=\"asmtMgmtForm\"]/div/div[1]/div/a[4]")
	public WebElement backButton;
	
	@FindBy(xpath="//*[@id=\"asmtMgmtForm\"]/div/div[3]/div[3]/div[1]/div[2]/a")
	public WebElement slug;
	

	@FindBy(xpath="//*[@id=\"asmtMgmtForm\"]/div/div[3]/div[1]/div/h3")
	public WebElement heading;

	@FindBy(xpath="//*[@id=\"asmt-delete-pop\"]/div[2]/div/div[3]/button")
	public WebElement noDelete;
	
	@FindBy (xpath="//*[@id=\"asmtMgmtForm:j_idt837\"]")
	public WebElement yesDelete;
	
	@FindBy (xpath="//*[@id=\"asmt-delete-pop\"]/div[2]/div/div[1]/div/button")
	public WebElement closeModal;
	
	@FindBy(xpath="//*[@id=\"asmtMgmtForm:msgId\"]/li")
	public WebElement deleteAccessDeniedMsg;
	
	//to get to assessment name page from assessments page, click this element
	@FindBy(xpath="//*[@id=\"asmtMgmtForm:asmtPubListTblId\"]/tbody/tr[4]/td[2]/a")//*[@id="asmtMgmtForm:asmtPubListTblId"]/tbody/tr[2]/td[2]/a
	public WebElement rankAssessment;

}
