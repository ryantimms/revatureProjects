<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<link href="background.css" rel="stylesheet">

<title>Sign in.</title>
</head>
<body>
	<div class="container">

		<form method="POST" class="form-signin"
			action="/EmployeeReimbursementSystem/Login.do">
			<h2 class="form-signin-heading">Please sign in</h2>
			<label for="username" class="sr-only">Username</label> <input
				type="text" name="inputUsername" class="form-control"
				placeholder="Username" required> <label for="password"
				class="sr-only">Password</label> <input type="password"
				name="inputPassword" class="form-control" placeholder="Password"
				required>

			<button class="btn btn-lg btn-primary btn-block" type="submit">Sign
				in</button>

			<a href="/EmployeeReimbursementSystem/newUser.jsp"
				class="btn btn-link">New user? Click here to create an account.</a>
		</form>

	</div>
</body>
</html>