package com.revature.cucumber;

import java.io.File;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.revature.factory.AssessmentsAddquestions;
import com.revature.factory.AssessmentsCreateNew;
import com.revature.factory.AssessmentsPage;
import com.revature.factory.HomePage;
import com.revature.factory.LoginPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AddQuestion {
	Properties properties = new Properties();
	WebDriver driver;

	@Given("^I am in Home page$")
	public void i_am_in_Home_page() throws Throwable {
		File configFile = new File("src/main/resources/config.properties");
		FileInputStream fileInput = new FileInputStream(configFile);
		properties.load(fileInput);
		fileInput.close();

		File file = new File("C:\\Automation\\chromedriver_win32\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://preview.revature.com/core/");
		LoginPage loginPage = new LoginPage(driver);
		loginPage.username.sendKeys(properties.getProperty("username"));
		loginPage.password.sendKeys(properties.getProperty("password"));
		loginPage.submit.click();
		while (driver.getTitle().equals("Revature | Admin")) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		System.out.println(driver.getTitle());
		Assert.assertNotEquals(driver.getTitle(), "Revature | Admin");
		// throw new PendingException();
	}

	@When("^I adds an question$")
	public void i_adds_an_question() throws Throwable {
		
		// fill in mandatory fields with what should be acceptable input
		// activity points and pass % have default values
		HomePage hm = new HomePage(driver);
		hm.AssessmentsLink.click();
		AssessmentsPage ap = new AssessmentsPage(driver);
		ap.createNewButton.click();
		AssessmentsCreateNew aCreateNew = new AssessmentsCreateNew(driver);

		aCreateNew.assessmentNameInput.clear();
		aCreateNew.assessmentNameInput.sendKeys(properties.getProperty("alphaTitle"));
		aCreateNew.tagsInput.clear();
		aCreateNew.tagsInput.sendKeys(properties.getProperty("JTA"));
		aCreateNew.tagsInput.sendKeys(Keys.RETURN);
		aCreateNew.tagsInput.sendKeys(properties.getProperty("RANK"));
		aCreateNew.tagsInput.sendKeys(Keys.RETURN);
		aCreateNew.activityPointsInput.clear();
		aCreateNew.activityPointsInput.sendKeys(properties.getProperty("Points"));
		aCreateNew.activityPointsInput.sendKeys(Keys.RETURN);

		aCreateNew.slugInput.clear();
		aCreateNew.slugInput.sendKeys(properties.getProperty("alphaTitle"));
		Select levelDropdowns = new Select(aCreateNew.levelDropdowns);
		levelDropdowns.selectByValue(properties.getProperty("level"));
		aCreateNew.categoryDropdown.click();
		aCreateNew.dotNetCategory.click();
		aCreateNew.levelOverrideOnRadio.click();
		aCreateNew.addQuestionsButton.click();
		
	}

	@When("^enter the mandatory fields to add to list$")
	public void enter_the_mandatory_fields_to_add_to_list() throws Throwable {
		// enter PoolName,click on CheckBox
		AssessmentsAddquestions qa = new AssessmentsAddquestions(driver);
		qa.PoolName.sendKeys(properties.getProperty("poolname"));
		qa.CheckBox.click();
		Assert.assertTrue(qa.CheckBox.isSelected(), "Checkbox selected");
		qa.AddToList.click();
		Assert.assertEquals(qa.alerttag.getText(), "Question(s) added successfully.");
		System.out.println("Added to list was successful");
		qa.AttachtoAssessment.click();
		qa.StickyCheckBox.click();
		// To check if the question has been added to the sticky questions
		System.out.println("Sticky check box is checked" + qa.StickyCheckBox.isSelected());
		System.out.println("Delete question button is enabled" + qa.DeleteQuestion.isEnabled());
		qa.AddQuestion.click();
		// System.out.println(qa.StickyAddedQuestionsTag.getText());
		// Assert.assertEquals(qa.StickyAddedQuestionsTag.getText(), "Added
		// Question(s)");
		// throw new PendingException();
	}

	@Then("^question gets added to the assessments page$")
	public void question_gets_added_to_the_assessments_page() throws Throwable {
		AssessmentsAddquestions qa = new AssessmentsAddquestions(driver);
		System.out.println(qa.StickyAddedQuestionsTag.getText());
		Assert.assertEquals(qa.StickyAddedQuestionsTag.getText(), "Added Question(s)");
		//qa.Questionlinkclick.click();
		
		HomePage homePage = new HomePage(driver);
		homePage.settingsDropdown.click();
		homePage.logOutButton.click();
		driver.quit();
	}

}