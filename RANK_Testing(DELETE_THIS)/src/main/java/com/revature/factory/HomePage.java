package com.revature.factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	public HomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//*[@id=\"asmtMgmt\"]/a")
	public WebElement AssessmentsLink;
	
	// If this doesnt work, change from <a dropdowntoggle> element
	@FindBy(xpath = "//*[@id=\"qsnMasterLnkId\"]/a/b")
	public WebElement QuestionsDropdown;

	@FindBy(xpath = "//*[@id=\"questionMgmt\"]/a")
	public WebElement AllQuestionsLink;

	@FindBy(xpath = "//*[@id=\"importMgmt\"]/a")
	public WebElement ImportQuestionLink;

	
	// Logout Functionality
	@FindBy(xpath = "//*[@id=\"settings\"]")
	public WebElement settingsDropdown;
	// Logout Functionality
	@FindBy(xpath = "//*[@id=\"mainFormMenu:log-out-BTN\"]")
	public WebElement logOutButton;
}
