package com.revature.ajax;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.revature.dao.ReimbursementDaoImp;
import com.revature.model.Reimbursement;
import com.revature.model.User;
import com.google.gson.Gson;
/**
 * Servlet implementation class EmployeeReimbServlet
 */
public class EmployeeReimbServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmployeeReimbServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User loggedUser=(User) request.getSession().getAttribute("loggedUser");
		List<Reimbursement> allUserReimbs = ReimbursementDaoImp.getInstance().selectSingleEmployeeAll(loggedUser);
		String json = new Gson().toJson(allUserReimbs);
		response.setContentType("application/json");
		response.getWriter().write(json);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
