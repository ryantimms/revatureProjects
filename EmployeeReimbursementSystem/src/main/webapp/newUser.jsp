<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Login page">
<meta name="author" content="Ryan">

<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<link href="background.css" rel="stylesheet">

<title>New User Registration</title>
</head>
<body>
	<div class="container">

		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-header">
				<a class="navbar-brand">Revature</a>
				<ul class="nav navbar-nav">
					<li><a href="#"></a></li>

					<li><a href="#"></a></li>

					<li><a href="#"></a></li>
				</ul>
			</div>
		</div>
		<br>
		<br>
		<br>
		<br>

		<form method="POST" action="/EmployeeReimbursementSystem/newUser.do">
			<div class="form-group">
				<label for="UserName">Username:</label> <input type="text"
					class="form-control" name="inputUsername" placeholder="Username">
			</div>
			<div class="form-group">
				<label for="password">Password:</label> <input type="password"
					class="form-control" name="inputPassword" placeholder="Password">
			</div>
			<div class="form-group">
				<label for="FirstName">First Name:</label> <input type="text"
					class="form-control" name="firstName" placeholder="First Name">
			</div>
			<div class="form-group">
				<label for="LastName">Last Name:</label> <input type="text"
					class="form-control" name="lastName" placeholder="Last Name">
			</div>
			<div class="form-group">
				<label for="Email">Email Address:</label> <input type="email"
					class="form-control" name="email" placeholder="Email">
			</div>
			<label for="sel1">User type(1 for Employee, 2 for Manager):</label> <input
				type="text" class="form-control" name="role" placeholder="1"/> <br> <br>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
		</form>
	</div>
</body>
</html>