package com.revature.dao;

import java.util.List;

import com.revature.model.User;

public interface UserDao {
	public boolean insert(User user);
	public boolean insertProc(User user);
	public boolean update(User user);
	public User getUser(User user);
	public User getUserByUserName(User user);
	public String getUserHash(User user);
	public List<User> getAllEmployees();
}	
