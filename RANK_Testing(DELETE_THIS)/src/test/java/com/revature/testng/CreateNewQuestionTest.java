package com.revature.testng;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.revature.factory.AllQuestions;
import com.revature.factory.AllQuestions_CreateNew;
import com.revature.factory.HomePage;
import com.revature.factory.LoginPage;

public class CreateNewQuestionTest {
	Properties properties = new Properties();
	WebDriver driver;

	@Test(priority = 3)
	public void normalTitleTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		createNewPage.questionTitle.sendKeys(properties.getProperty("newQuestionTitle"));
		Assert.assertEquals(createNewPage.questionTitle.getAttribute("value"),
				properties.getProperty("newQuestionTitle"));
	}

	@Test(priority = 4)
	public void questionCategoryTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		Select categories = new Select(createNewPage.category);
		createNewPage.category.sendKeys(Keys.ARROW_DOWN);
		Assert.assertEquals(categories.getFirstSelectedOption().getText(),
				properties.getProperty("newQuestionCategory"));
	}

	@Test(priority = 2)
	public void levelTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		Select levels = new Select(createNewPage.level);
		createNewPage.level.sendKeys(Keys.ARROW_DOWN);
		Assert.assertEquals(levels.getFirstSelectedOption().getText(), properties.getProperty("newQuestionLevel"));
	}

	@Test(priority = 1)
	public void questionTypeTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		Select types = new Select(createNewPage.questionType);
		// BestChoice
		types.selectByIndex(1);
		Assert.assertTrue(createNewPage.bestChoiceTable.isDisplayed());
		// True/False
		types.selectByIndex(2);
		Assert.assertTrue(createNewPage.trueRadio.isDisplayed());
		Assert.assertTrue(createNewPage.falseRadio.isDisplayed());
		// MultipleChoice
		types.selectByIndex(3);
		Assert.assertTrue(createNewPage.bestChoiceTable.isDisplayed());
		// NumericalValue
		types.selectByIndex(4);
		Assert.assertTrue(createNewPage.numericalAnswer.isDisplayed());
	}

	@Test(priority = 5)
	public void skillPointsMaxValueTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		for (int i = 0; i < 20; i++) {
			createNewPage.skillPoints.sendKeys("1");
		}
		Assert.assertNotEquals(createNewPage.skillPoints.getAttribute("value").length(), 20);
	}

	@Test(priority = 6)
	public void skillPointsDecimalTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		createNewPage.skillPoints.clear();
		createNewPage.skillPoints.sendKeys(properties.getProperty("decimalNumber"));
		Assert.assertNotEquals(createNewPage.skillPoints.getAttribute("value"),
				properties.getProperty("decimalNumber"));
	}

	@Test(priority = 7)
	public void questionDurationTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		createNewPage.duration.click();
		createNewPage.incrementMinute.click();
		createNewPage.incrementHour.click();
		Assert.assertEquals(createNewPage.duration.getAttribute("value"), properties.getProperty("testDuration"));
	}

	@Test(groups = { "formCheckGroup" }, priority = 10)
	public void emptyTitleTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		int attempts = 0;
		setUp();
		while (attempts < 2) {
			try {
				createNewPage.questionTitle.clear();
				createNewPage.saveButton.click();
			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}
			attempts++;
		}
		// Asserts that the page will not change URL when save is clicked because the
		// title is empty
		Assert.assertEquals(driver.getCurrentUrl(), properties.getProperty("newQuestionURL"));

	}

	@Test(groups = { "formCheckGroup" }, priority = 11)
	public void emptyCategoryTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		int attempts = 0;
		setUp();
		// Catch Stale Element Exception and try again
		while (attempts < 2) {
			try {
				// Reset Category to the 'Select' option
				Select categories = new Select(createNewPage.category);
				categories.selectByIndex(0);
				createNewPage.saveButton.click();
			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}
			attempts++;
		}
		Assert.assertEquals(driver.getCurrentUrl(), properties.getProperty("newQuestionURL"));

	}

	@Test(groups = { "formCheckGroup" }, priority = 12)
	public void emptyLevelTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		int attempts = 0;
		setUp();
		// Catch Stale Element Exception and try again
		while (attempts < 2) {
			try {
				Select levels = new Select(createNewPage.level);
				levels.selectByIndex(0);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				createNewPage.saveButton.click();
			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}
			attempts++;
		}
		Assert.assertEquals(driver.getCurrentUrl(), properties.getProperty("newQuestionURL"));
	}

	@Test(groups = { "formCheckGroup" }, priority = 13)
	public void emptyPointsTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		int attempts = 0;
		setUp();
		// Catch Stale Element Exception and try again
		while (attempts < 2) {
			try {
				createNewPage.skillPoints.clear();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				createNewPage.saveButton.click();
			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}
			attempts++;
		}
		Assert.assertEquals(driver.getCurrentUrl(), properties.getProperty("newQuestionURL"));
	}

	@Test(groups = { "formCheckGroup" }, priority = 14)
	public void emptyScoreTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		int attempts = 0;
		setUp();
		// Catch Stale Element Exception and try again
		while (attempts < 2) {
			try {
				createNewPage.score.clear();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				createNewPage.saveButton.click();
			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}
			attempts++;
		}
		Assert.assertEquals(driver.getCurrentUrl(), properties.getProperty("newQuestionURL"));
	}

	@Test(groups = { "formCheckGroup" }, priority = 15)
	public void noDurationTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		int attempts = 0;
		setUp();
		// Catch Stale Element Exception and try again
		while (attempts < 2) {
			try {
				createNewPage.duration.click();
				createNewPage.decrementMinute.click();
				createNewPage.decrementMinute.click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				createNewPage.saveButton.click();
			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}
			attempts++;
		}
		Assert.assertEquals(driver.getCurrentUrl(), properties.getProperty("newQuestionURL"));

	}

	@Test(groups = { "formCheckGroup" }, priority = 16)
	public void noTagTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		int attempts = 0;
		setUp();
		// Catch Stale Element Exception and try again
		while (attempts < 2) {
			try {
				 createNewPage.tagsDiv.clear();
				createNewPage.tags.clear();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				createNewPage.saveButton.click();
			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}
			attempts++;
		}
		Assert.assertEquals(driver.getCurrentUrl(), properties.getProperty("newQuestionURL"));
	}

	@Test(groups = { "formCheckGroup" }, priority = 17)
	public void zeroScoreTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		int attempts = 0;
		setUp();
		// Catch Stale Element Exception and try again
		while (attempts < 2) {
			try {
				createNewPage.score.clear();
				createNewPage.score.sendKeys(Keys.NUMPAD0);
				createNewPage.saveButton.click();
			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}
			attempts++;
		}
		Assert.assertEquals(driver.getCurrentUrl(), properties.getProperty("newQuestionURL"));
	}
	
	@Test(groups= {"formCheckGroup"}, priority=18)
	public void numericalAnswerEmptyTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		int attempts = 0;
		setUp();
		// Catch Stale Element Exception and try again
		while (attempts < 2) {
			try {
				Select types=new Select(createNewPage.questionType);
				types.selectByIndex(4);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				createNewPage.numericalAnswer.clear();
				createNewPage.saveButton.click();
			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}
			attempts++;
		}
		Assert.assertEquals(driver.getCurrentUrl(), properties.getProperty("newQuestionURL"));
	}

	@Test(groups = { "formCheckGroup" }, priority = 18)
	public void zeroSkillPointTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		int attempts = 0;
		setUp();
		// Catch Stale Element Exception and try again
		while (attempts < 2) {
			try {
				createNewPage.skillPoints.clear();
				createNewPage.skillPoints.sendKeys(Keys.NUMPAD0);
				createNewPage.saveButton.click();
			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}
			attempts++;
		}
		Assert.assertEquals(driver.getCurrentUrl(), properties.getProperty("newQuestionURL"));
	}

	@Test(groups= {"multipleChoiceGroup"}, priority=19)
	public void mChoiceAddAnswerTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		int attempts = 0;
		setUp();
		// Catch Stale Element Exception and try again
		while (attempts < 2) {
			try {
				Select types = new Select(createNewPage.questionType);
				types.selectByIndex(3);
				createNewPage.mChoiceAddAnswerButton.click();
			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}
			attempts++;
		}
		Assert.assertTrue(createNewPage.mChoiceDeleteAnswerButton.isDisplayed());
	}
	
	@Test(groups= {"multipleChoiceGroup"}, priority=20)
	public void mChoiceChangePercentageTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		int attempts = 0;
		setUp();
		// Catch Stale Element Exception and try again
		while (attempts < 2) {
			try {
				Select types = new Select(createNewPage.questionType);
				types.selectByIndex(3);
				createNewPage.mChoicePercentScrollUpBtn.click();
			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}
			attempts++;
		}
		Assert.assertEquals(createNewPage.mChoicePercentTextbox.getAttribute("value"), "15");
		attempts=0;
		while (attempts < 2) {
			try {
				createNewPage.mChoicePercentScrollDownBtn.click();
			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}
			attempts++;
		}
		Assert.assertEquals(createNewPage.mChoicePercentTextbox.getAttribute("value"), "5");
		
		createNewPage.mChoiceYesCheckbox.click();
		while (attempts < 2) {
			try {
				//Uncheck the 'Yes' checkbox
				//This means the percentage should be <= 0 because
				//it is an incorrect choice
				createNewPage.mChoicePercentScrollUpBtn.click();
			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}
			attempts++;
		}
		Assert.assertEquals(createNewPage.mChoicePercentTextbox.getAttribute("value"), "0");
	}
	
	@Test(groups= {"multipleChoiceGroup"}, priority=21)
	public void deleteButtonTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		int attempts = 0;
		setUp();
		// Catch Stale Element Exception and try again
		while (attempts < 2) {
			try {
				Select types = new Select(createNewPage.questionType);
				types.selectByIndex(3);
				createNewPage.mChoiceAddAnswerButton.click();
				createNewPage.mChoiceDeleteAnswerButton.click();
			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}
			attempts++;
		}
		Assert.assertEquals(createNewPage.rows.size(), 3);
	}
	
	@Test(groups= {"multipleChoiceGroup"}, priority=22)
	public void bestChoiceNoInputsTest() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		int attempts = 0;
		setUp();
		// Catch Stale Element Exception and try again
		while (attempts < 2) {
			try {
				Select types=new Select(createNewPage.questionType);
				types.selectByIndex(1);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				createNewPage.saveButton.click();
			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}
			attempts++;
		}
		Assert.assertEquals(driver.getCurrentUrl(), properties.getProperty("newQuestionURL"));
	}
	
	
	
	@BeforeGroups(groups = {"formCheckGroup", "multipleChoiceGroup"})
	public void setUp() {
		AllQuestions_CreateNew createNewPage = new AllQuestions_CreateNew(driver);
		driver.navigate().refresh();
		int attempts = 0;
		while (attempts < 2) {
			try {
				createNewPage.questionType.sendKeys(Keys.ARROW_DOWN);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				createNewPage.questionTitle.sendKeys(properties.getProperty("newQuestionTitle"));
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				createNewPage.category.sendKeys(Keys.ARROW_DOWN);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				createNewPage.level.sendKeys(Keys.ARROW_DOWN);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				createNewPage.skillPoints.sendKeys(properties.getProperty("skillPoints"));
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				createNewPage.score.sendKeys(properties.getProperty("score"));
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				createNewPage.duration.click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				createNewPage.decrementMinute.click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				createNewPage.tags.sendKeys(properties.getProperty("newQuestionTag"));
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

				Select types = new Select(createNewPage.questionType);
				// Selects True/False as question type
				types.selectByIndex(2);

				createNewPage.trueRadio.click();
				createNewPage.trueExplanation.sendKeys(properties.getProperty("newQuestionTrueExp"));
				createNewPage.falseExplanation.sendKeys(properties.getProperty("newQuestionFalseExp"));

				createNewPage.tags.sendKeys(Keys.RETURN);

			} catch (StaleElementReferenceException e) {
			} catch (WebDriverException e) {
			}

			attempts++;
		}
	}

	@Test(dataProvider = "dp")
	public void f(Integer n, String s) {
	}

	@BeforeMethod
	public void beforeMethod() {
	}

	@AfterMethod
	public void afterMethod() {
	}

	@DataProvider
	public Object[][] dp() {
		return new Object[][] { new Object[] { 1, "a" }, new Object[] { 2, "b" }, };
	}

	@BeforeClass
	public void beforeClass() throws IOException {
		File configFile = new File("src/main/resources/config.properties");
		FileInputStream fileInput = new FileInputStream(configFile);
		properties.load(fileInput);
		fileInput.close();

		File file = new File("C:\\Automation\\chromedriver_win32\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("http://preview.revature.com/core/");
		LoginPage loginPage = new LoginPage(driver);
		loginPage.username.clear();
		loginPage.password.clear();
		loginPage.username.sendKeys(properties.getProperty("username"));
		loginPage.password.sendKeys(properties.getProperty("password"));
		loginPage.submit.click();
		while (driver.getTitle().equals(properties.getProperty("loginPageTitle"))) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		HomePage hPage = new HomePage(driver);
		hPage.QuestionsDropdown.click();
		hPage.AllQuestionsLink.click();
		while (driver.getTitle().equals(properties.getProperty("homePageTitle"))) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		AllQuestions aQPage = new AllQuestions(driver);
		while (driver.getCurrentUrl().equals(properties.getProperty("allQuestionsURL"))) {
			aQPage.CreateNew.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

	@BeforeTest
	public void beforeTest() {
	}

	@AfterTest
	public void afterTest() {
	}

	@BeforeSuite
	public void beforeSuite() {
	}

	@AfterSuite
	public void afterSuite() {
	}

}
